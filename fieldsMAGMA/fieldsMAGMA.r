#load fields
#library(fields)
library('fields', lib.loc='~/')

#load all MAGMA-accelerated functions
thisDir = "~/git/r-timing-code/fieldsMAGMA"
source(paste0(thisDir, '/RwrapperMAGMA.r'), chdir=TRUE)
source(paste0(thisDir, '/accelMKrig/mKrigMAGMA.r'), chdir=TRUE)
source(paste0(thisDir, '/accelMKrig.MLE/mKrigMAGMA.MLE.r'), chdir=TRUE)
