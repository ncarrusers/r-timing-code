#The following script computes how long the Krig and mKrig takes for many sized matrices
#using the CO2 dataset from fields.  Accelerated using 1 GPU. Uses chordal distances.

workflow = function(lonlat, y) {
  theta = 8
  
  print('optimizing over lambda')
  #now calculate likelihood for different lambdas using mKrig (requires lat and long data):
  lambda = 10^seq(-5, 0, length=10)
  
  out = mKrigMAGMA.MLE(lonlat, y, theta=theta, lambda=lambda, cov.args= list(Covariance="Exponential", Distance="rdist"), lambda.profile=FALSE, nGPUs=1, singlePrecision=TRUE)
  lnLikes = out$summary[,2]
  
  #use spline interpolator to find max likelihood:
  interpGrid = 10^(seq(-5, 0, length=150))
  interpLnLikes = splint(lambda, lnLikes, interpGrid)
  index = which.max(interpLnLikes)
  lambdaMLE <- interpGrid[index]
  
  #compute MLE krig:
  out.mle <- mKrigMAGMA(lonlat, y, theta=theta, lambda=lambdaMLE, cov.args= list(Covariance="Exponential", Distance="rdist"), nGPUs=1, singlePrecision=TRUE)
  
  #Now predict surfaces and exact errors using Krig and mKrig objects:
  #NOTE: computing Kriging surfaces doesn't work for 3D coordinates
  print('computing Kriging surfaces...')
  out.p <- predictSurface(out.mle)
  
  #predict approximate surfaces and errors using mKrig:
  print('approximating Kriging surface')
  minLat = min(lonlat[,2])
  maxLat = max(lonlat[,2])
  size = maxLat - minLat
  gridExpansion = max(c(1 + 1e-07, 10*theta/size))

  #make sure grid is at least 10 times as big as theta
  tmp <- try(sim.mKrig.approx(out.mle, M=5, gridExpansion=gridExpansion))
  if(class(tmp) == 'try-error') {
    print('Used extra large gridExpansion')
    out.sim <- sim.mKrig.approx(out.mle, M=5, gridExpansion=2*gridExpansion)
    print('Finished using extra large grid expansion')
  } else {
    out.sim <- tmp
  }
  
  invisible(NULL)
}

#load accelerated fields
#library(fields)
library('fields', lib.loc='~/')

myDir = "~/git/r-timing-code/fieldsMAGMA/testing/workflow"
source(paste0(myDir, "/../../fieldsMAGMA.r"))
myDir = "~/git/r-timing-code/fieldsMAGMA/testing/workflow"

#now time the workflow for Krig or mKrig using CO2 dataset from fields:
data(CO2)

maxSize = 24000 # maximum matrix size (number of observations) for timing
limIter = 3   # how far to increase lat/lon limits on CO2 data per iteration
lim <- 15     # starting limit for how many degrees lat/lon points can be from each other
i = 1

lims = c()
ns = c()
times = c()
while(T) {
  #only use a subset of the data that is close enough to 0 latitude and 0 longitude:
  ind <- (-lim < CO2$lon.lat[,1]) & (CO2$lon.lat[,1] < lim) # only include pts with lon coords between -Xlim and Xlim
  ind <- (ind & -lim/2 < CO2$lon.lat[,2]) & (CO2$lon.lat[,2] < lim/2) # only include pts with lat coords between -Xlim/2 and Xlim/2
  
  lonlat = CO2$lon.lat[ind,]
  y <- CO2$y[ind]
  n = length(y)
  print(paste0('Using ', n, ' data points with lim: ', lim))
  
  # if there are too many points in the matrix or all the points on the earth are covered, break:
  if(n > maxSize || lim -limIter >= 180) {
    break
  } 
  #if there's the same number of points as last time, continue:
  else if(i > 1 && n == ns[i - 1]) {
    lim = lim + limIter
    next
  }
  
  # else, record timings:
  ns[i] = n
  lims[i] = lim
  
  times[i] = system.time(workflow(lonlat, y))[3]
  print(paste0("Time: ", times[i]))
  
  # iterate:
  lim = lim + limIter
  i = i + 1
}

#save data:
save(list=c("ns", "times", "lims"), file=paste0(myDir, "/accelMKrigWorkflowTimes1s_n", maxSize, ".RData"))
