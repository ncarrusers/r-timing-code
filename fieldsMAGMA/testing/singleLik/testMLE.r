#set random seed
seed = 1

#assumes exponential covariance model
genCovMat = function(x, theta, lambda) {
  distanceMatrix<- rdist(x,x)
  Sigma<- exp( -distanceMatrix/theta ) + diag(x=lambda, nrow=nrow(distanceMatrix))
  return(Sigma)
}

#load accelerated fields
#library(fields)
library('fields', lib.loc='~/')

myDir = "~/git/r-timing-code/fieldsMAGMA/testing/singleLik"
source(paste0(myDir, "/../../fieldsMAGMA.r"))
myDir = "~/git/r-timing-code/fieldsMAGMA/testing/singleLik"

ns = c(5000, 10000)
nReps = c(2, 1)
thetas= 5^seq(from=log(.01, 5), to=1.2, length=15)
lambdas = 5^seq(from=log(.01), to=1.2, length=15)

for(i in 1:length(ns)) {
  n = ns[i]
  nRep = nReps[i]
  
  logLik.true.single = array(dim=c(nRep, length(thetas), length(lambdas)))
  logLik.true.double = array(dim=c(nRep, length(thetas), length(lambdas)))
  logLik.MLE.single = array(dim=c(nRep, length(thetas), length(lambdas)))
  logLik.MLE.double = array(dim=c(nRep, length(thetas), length(lambdas)))
  for(j in 1:length(thetas)) {
    trueTheta = thetas[j]
    
    for(k in 1:length(lambdas)) {
      trueLambda = lambdas[k]
      r = 1
      errCount = 0
      
      while(r <= nRep) {
        print(paste0("n: ", n, " theta: ", trueTheta, " lambda: ", trueLambda, " rep: ", r))
        if(errCount >= nRep) {
          print("Too many errors, skipping to next parameter set")
          break
        }
        
        #iterate and set random seed
        seed = seed + 1
        set.seed(seed)
        
        # if necessary generate observation locations, covariance matrix
        if(errCount != 0 || r==1) {
          x = matrix(runif(2*n), nrow=n)
          Sigma = genCovMat(x, trueTheta, trueLambda)
          
          #generate observations at locations
          U = magmaChol(Sigma, nGPUs=2)
          y = t(U)%*%as.vector(rnorm(n))
        }
        #
        #In the following mKrig calculations, if a matrix is singular for single but not double precision,
        #print information.  If either single or double matrix is singular, retry for different
        #observation set
        #
        
        ## set MLE sampling grid
        testThetas = seq(from=trueTheta-trueTheta/2, to=trueTheta+trueTheta/2, length=5)
        par.grid=list(theta=testThetas)
        
        ##calculate true log Likelihood double-precision
        outD <- try(mKrigMAGMA(x, y, cov.args= list(Covariance="Exponential", Distance="rdist"),
                                theta=trueTheta, lambda=trueLambda, find.trA=FALSE, nGPUs=2))
        if(class(outD) == 'try-error') {
          print(paste0('True parameter double-precision singular matrix error: ', outD))
          errCount = errCount + 1
          next
        }
        else
          logLik.true.double[r, j, k] = outD$lnProfileLike
        
        ##calculate true log Likelihood single-precision
        outS <- try(mKrigMAGMA(x, y, cov.args= list(Covariance="Exponential", Distance="rdist"),
                                theta=trueTheta, lambda=trueLambda, find.trA=FALSE, nGPUs=2, singlePrecision=TRUE))
        if(class(outS) == 'try-error') {
          print(paste0("True parameter single precision error with no double precision error for: n: ",
                       n, " theta: ", trueTheta, " lambda: ", trueLambda, " rep: ", r))
        }
        else
          logLik.true.single[r, j, k] = outS$lnProfileLike
        
        ##calculate MLE with double precision
        outD <- try(mKrigMAGMA.MLE(x, y, lambda=trueLambda, par.grid=par.grid,
                                    cov.args= list(Covariance="Exponential", Distance="rdist"), nGPUs=2))
        if(class(outD) == 'try-error') {
          print(paste0('MLE double-precision singular matrix error: ', outD))
          errCount = errCount + 1
          next
        } else {
          lambda.MLE.double = outD$lambda.MLE
          theta.MLE.double = outD$cov.args.MLE$theta
          
          #get MLE log likelihood for double precision
          thetaInd = which(testThetas == theta.MLE.double, TRUE)[1]
          lambdaInd = which(outD$lnLike.eval[[thetaInd]][,1] == lambda.MLE.double, TRUE)[1]
          logLik.MLE.double[r, j, k] = outD$lnLike.eval[[thetaInd]][lambdaInd, 4]
        }
        
        ##calculate MLE with single precision
        outS <- try(mKrigMAGMA.MLE(x, y, lambda=trueLambda, par.grid=par.grid,
                                    cov.args= list(Covariance="Exponential", Distance="rdist"), nGPUs=2, singlePrecision=TRUE))
        if(class(outS) == 'try-error') {
          print(paste0("MLE single precision error with no double precision error for: n: ",
                       n, " theta: ", trueTheta, " lambda: ", trueLambda, " rep: ", r))
          next
        } else {
          lambda.MLE.single = outS$lambda.MLE
          theta.MLE.single = outS$cov.args.MLE$theta
          
          #get MLE log likelihood for single precision
          thetaInd = which(testThetas == theta.MLE.single)[1]
          lambdaInd = which(outS$lnLike.eval[[thetaInd]][,1] == lambda.MLE.single)[1]
          lambdaInd = lambdaInd[1]
          logLik.MLE.single[r, j, k] = outS$lnLike.eval[[thetaInd]][lambdaInd, 4]
        }
        
        #print results of this loop and iterate
        print(paste0("logLik.true.single: ", logLik.true.single[r, j, k], " logLik.true.double: ", logLik.true.double[r, j, k]))
        print(paste0("logLik.MLE.single: ", logLik.MLE.single[r, j, k], " logLik.MLE.double: ", logLik.MLE.double[r, j, k]))
        r=r+1
      }
    }
  }
  
  #save data:
  save(list=c("n", "nRep", "thetas", "lambdas", "logLik.true.single", "logLik.true.double", "logLik.MLE.single",
              "logLik.MLE.double"), file=paste0(myDir, "/testMLE_n", n, ".RData"))
}
