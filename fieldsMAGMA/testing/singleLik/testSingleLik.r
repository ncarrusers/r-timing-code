#assumes exponential covariance model
genCovMat = function(x, theta, lambda) {
  distanceMatrix<- rdist(x,x)
  Sigma<- exp( -distanceMatrix/theta ) + diag(x=lambda, nrow=nrow(distanceMatrix))
  return(Sigma)
}

#based on Doug's intro paper and my siparcs presentation
calcLogLik = function(U, y) {
  lnDet = 2*sum(log(diag(U)))
  u = backsolve(U, y)
  leftTerm = sum(u^2)
  return((leftTerm + lnDet)/2)
}

#load accelerated fields
#library(fields)
library('fields', lib.loc='~/')

myDir = "~/git/r-timing-code/fieldsMAGMA/testing/singleLik"
source(paste0(myDir, "/../../fieldsMAGMA.r"))
myDir = "~/git/r-timing-code/fieldsMAGMA/testing/singleLik"

maxSize = 20000 # maximum matrix size (number of observations) for timing
ns = seq(from=20000, to=maxSize, by=5000)
nReps = c(3)
thetas= 5^seq(from=-2, to=1, length=15)
lambdas = 5^seq(-5, 1, length=15)

for(i in 1:length(ns)) {
  n = ns[i]
  
  pctError = array(dim=c(nReps[i], length(thetas), length(lambdas)))
  error = array(dim=c(nReps[i], length(thetas), length(lambdas)))
  for(j in 1:length(thetas)) {
    trueTheta = thetas[j]
    
    for(k in 1:length(lambdas)) {
      trueLambda = lambdas[k]
      
      for(r in 1:nReps[i]) {
        print(paste0("n: ", n, " theta: ", trueTheta, " lambda: ", trueLambda, " rep: ", r))
        
        # generate observation locations, covariance matrix
        x = matrix(runif(2*n), nrow=n)
        Sigma = genCovMat(x, trueTheta, trueLambda)
        
        #generate observations at locations
        U = magmaChol(Sigma, nGPUs=2)
        y = t(U)%*%as.vector(rnorm(n))
        
        #calculate likelihoods
        singleOut = mKrigMAGMA(x, y, cov.args= list(Covariance="Exponential", Distance="rdist"),
                                theta=trueTheta, lambda=trueLambda, find.trA=FALSE,
                                nGPUs=2, singlePrecision=TRUE)
        doubleOut = mKrigMAGMA(x, y, cov.args= list(Covariance="Exponential", Distance="rdist"),
                                theta=trueTheta, lambda=trueLambda, find.trA=FALSE,
                                nGPUs=2)
        singleLnLik = singleOut$lnProfileLike
        doubleLnLik = doubleOut$lnProfileLike
        error[r,j,k] = singleLnLik - doubleLnLik
        pctError[r,j,k] = error[r,j,k]/doubleLnLik
        
        print(paste0("pctError: ", pctError[r,j,k], " error: ", error[r,j,k]))
      }
    }
  }
  #save data:
  save(list=c("n", "r", "thetas", "lambdas", "pctError", "error"), file=paste0(myDir, "/testSingleLik_n", n, ".RData"))
}
