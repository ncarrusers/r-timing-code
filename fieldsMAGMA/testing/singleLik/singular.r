#load accelerated fields
#library(fields)
library('fields', lib.loc='~/')

myDir = "~/git/r-timing-code/fieldsMAGMA/testing/singleLik"
source(paste0(myDir, "/../../fieldsMAGMA.r"))
myDir = "~/git/r-timing-code/fieldsMAGMA/testing/singleLik"

n = 10000
thetas = 10^(1:17)
trueLambda = 1

minCov = array(dim=c(length(thetas)))
singular.double = array(FALSE, dim=c(length(thetas)))
singular.single = array(FALSE, dim=c(length(thetas)))
qrSingular.double = array(FALSE, dim=c(length(thetas)))
qrSingular.single = array(FALSE, dim=c(length(thetas)))

#generate observations, dist mat, diagonal mat
width = sqrt(n)
xgrid<- seq(0, 1, length=width)
ygrid<- xgrid
x<- make.surface.grid( list(x= xgrid, y=ygrid))
negDistMat = -rdist(x, x)
diagMat = diag(trueLambda, nrow=n, ncol=n)

for(k in 1:length(thetas)) {
  trueTheta = thetas[k]
  
  #generate cov mat
  covMat = exp(negDistMat/trueTheta) + diagMat
  
  # get minimum covariance
  minCov[k] = min(covMat)
  
  #print run info
  print(paste0("n: ", n, " lambda: ", trueLambda, " minCov: ", minCov[k]))
  
  #generate observations at locations
  if(singular.double[k] != TRUE) {
    UDouble = try(magmaChol(covMat, nGPUs=2))
    if(class(UDouble) == 'try-error') {
      print(paste0('Matrix is singular in double precision: ', UDouble))
      singular.double[k:length(thetas)] = TRUE
      qrSingular.double[k:length(thetas)] = TRUE
      singular.single[k:length(thetas)] = TRUE
      qrSingular.single[k:length(thetas)] = TRUE
      break
    }
  }
  if(singular.single[k] != TRUE) {
    USingle = try(magmaChol(covMat, nGPUs=2, singlePrecision=TRUE))
    if(class(USingle) == 'try-error') {
      print(paste0('Matrix is singular in single precision: ', USingle))
      singular.single[k:length(thetas)] = TRUE
      qrSingular.single[k:length(thetas)] = TRUE
    }
  }
  y = t(UDouble)%*%as.vector(rnorm(n))
  
  #calculate likelihoods
  if(qrSingular.double[k] != TRUE) {
    tmp = try(mKrigMAGMA(x, y, cov.args= list(Covariance="Exponential", Distance="rdist"),
                          theta=trueTheta, lambda=trueLambda, find.trA=FALSE,
                          nGPUs=2))
    if(class(tmp) == 'try-error') {
      print(paste0('Matrix is QR-singular in single precision: ', tmp))
      qrSingular.double[k:length(thetas)] = TRUE
      qrSingular.single[k:length(thetas)] = TRUE
      next
    }
  }
  if(qrSingular.single[k] != TRUE) {
    tmp = try(mKrigMAGMA(x, y, cov.args= list(Covariance="Exponential", Distance="rdist"),
                          theta=trueTheta, lambda=trueLambda, find.trA=FALSE,
                          nGPUs=2, singlePrecision=TRUE))
    if(class(tmp) == 'try-error') {
      print(paste0('Matrix is QR-singular in single precision: ', tmp))
      qrSingular.single[k:length(thetas)] = TRUE
    }
  }
}

#save data:
save(list=c("n", "trueLambda", "thetas", "minCov", "singular.single", "singular.double",
            "qrSingular.single", "qrSingular.double"), 
     file=paste0(myDir, "/singular_n", n, ".RData"))
