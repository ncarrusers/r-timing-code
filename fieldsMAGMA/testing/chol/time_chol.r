library(fields)

#This function times how long a cholesky decomposition takes for an n x n covariance matrix
#Assumes data is in grid and a scale parameter of .2 for an exponential covariance model
timeGridChol = function(n) {
  # get submatrix for the n points
  d = round(sqrt(n))
  subMat = Sigma[1:n, 1:n]
  return(system.time(chol(subMat), gcFirst=T)[3])
}

# generate grid for sampling
maxSize = 35000
M = round(sqrt(maxSize), digits=-1)
xgrid<- seq(0, 1, length=M)
ygrid<- xgrid
x<- make.surface.grid( list(x= xgrid, y=ygrid))

# matrix of pairwise distances
distanceMatrix<- rdist(x,x)
# an exponential covariance function with range parameter theta=.2
theta=.2
Sigma<- exp( -distanceMatrix/theta )

ns = seq(10, M, by=5)^2
times = ns
for(i in 1:length(ns)) {
  times[i] = timeGridChol(ns[i])
  print(paste0("n: ", ns[i], ", time: ", times[i]))
}

thisDir = "~/git/r-timing-code/fields_MAGMA/testing/chol"
save(list=c("ns", "times"), file=paste0(thisDir, "/cholTimes_n", maxSize, ".RData"))
