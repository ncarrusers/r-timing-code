library(Rmpi)
source('~/code/testing/rmpi/tutorial/brute_force.r')

maxCores = 16 
times = double(maxCores-2)
commTimes = double(maxCores-2)
errors = logical(maxCores-2)
ns = 3:maxCores
for(i in 1:(maxCores-2)) {
  times[i] = system.time(commTimes[i] <- brute_force(i+2))[3]
}

save(list=c('ns', "times", 'commTimes'), file="~/code/testing/rmpi/tutorial/brute_force_times.RData")
mpi.quit()
