library(Rmpi)
source('~/code/testing/rmpi/startup/batchFile.R')

maxCores = 16 
times = double(maxCores-2)
commTimes = double(maxCores-2)
errors = logical(maxCores-2)
ns = 3:maxCores
for(i in 1:(maxCores-2)) {
  times[i] = system.time(commTimes[i] <- batchFile(i+2))[1]
}

save(list=c('ns', "times", 'commTimes'), file="~/code/testing/rmpi/tutorial/batchFile_times.RData")
mpi.quit()
