# A function that calls lambdaKrig.R on multiple mpi cores

#########################
# Prepare the master node
#########################

# Load packages, source functions, etc.
require('spam', lib.loc='/glade/u/home/jpaige/code/')
require('maps', lib.loc='/glade/u/home/jpaige/code/')
require('fields', lib.loc='/glade/u/home/jpaige/code/')
library(Rmpi)
library(ncdf4)
source("/glade/u/home/jpaige/code/testing/rmpi/startup/lambdaKrig.R")

# A couple parameters to allow easier testing
batchFile = function(procs) {
N <- 50

#########################################
# Perform any code needed for every slave
#########################################

# Change to the data dir and read in the files
setwd("/glade/p/work/mcginnis/mica/data/daily/narccap/tmax")
nameList <- system("ls", intern=TRUE)

# Read in the orography file to pass to the functions
orogfile <- "/glade/p/work/mcginnis/mica/orog.grid.nc"
fin  <- nc_open(orogfile) 
glat <- ncvar_get(fin,"lat")
glon <- ncvar_get(fin,"lon")
orog <- ncvar_get(fin,"orog")

# Construct 2-D lat/lon arrays

olat <- t(matrix(glat,length(glat),length(glon)))
olon <-   matrix(glon,length(glon),length(glat))

xout <- cbind(c(olon), c(olat))

nc_close(fin)

############
# Rmpi it up!
############

# Spawn the slaves
commTime = system.time(mpi.spawn.Rslaves(nslaves=procs-1, needlog=F))[1]

# Load packages on the slaves
commTime = commTime + system.time(mpi.bcast.cmd(library(fields)))[1]
commTime = commTime + system.time(mpi.bcast.cmd(library(ncdf4)))[1]

# Pass variables needed by the slaves (there has to be a cleaner way...)
commTime = commTime + system.time(mpi.bcast.Robj2slave(lambdaKrig))[1]
commTime = commTime + system.time(mpi.bcast.Robj2slave(nameList))[1]
commTime = commTime + system.time(mpi.bcast.Robj2slave(glat))[1]
commTime = commTime + system.time(mpi.bcast.Robj2slave(glon))[1]
commTime = commTime + system.time(mpi.bcast.Robj2slave(orog))[1]
commTime = commTime + system.time(mpi.bcast.Robj2slave(olat))[1]
commTime = commTime + system.time(mpi.bcast.Robj2slave(olon))[1]
commTime = commTime + system.time(mpi.bcast.Robj2slave(xout))[1]
commTime = commTime + system.time(mpi.bcast.Robj2slave(blah <- 1:N))[1] #to take into acct mpi.iapply communication

# Run the main function. This returns a list with each index containing
# an itereation of apply
result <- mpi.iapplyLB(1:N,lambdaKrig)

# Don't forget to save!
# save(result, file="/glade/scratch/lenssen/saveTestM24/mpiTestDetails.Rdata")

# Close slaves and quit cleanly out of R/Rmpi
commTime = commTime + system.time(mpi.close.Rslaves())[1]
return(commTime)
}
