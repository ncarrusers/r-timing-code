#include <stdlib.h>
#include <stdio.h>
#include <R.h>
#include <Rdefines.h>

#include <plasma.h>
#include <cblas.h>
#include <lapacke.h>
#include <core_blas.h>
#include <quark.h>
#include <quark_unpack_args.h>

SEXP plasmaEigen(SEXP A, SEXP w, SEXP Q, SEXP n, SEXP cores, SEXP NB, SEXP IB)
{
	int In, Icores, status, INB, IIB;
	double *PA, *Pw, *PQ;
	In = INTEGER_VALUE(n);
	Icores = INTEGER_VALUE(cores);
	INB = INTEGER_VALUE(NB);
	IIB = INTEGER_VALUE(IB);
	PA = NUMERIC_POINTER(A);
	Pw = NUMERIC_POINTER(w);
	PQ = NUMERIC_POINTER(Q);
	int i,j,offset1,offset2,offset3;
	for(i = 0; i < In*In; i++)
	{
		if(PA[i] > .0000000001)
		{
			offset1 = i;
			break;
		}
	}
	
	for(j=0; j< In; j++)
	{
		if(Pw[j] ==99)
		{
			offset2 = j;
			break;
		}
	}
	for(i=0; i < In; i++)
	{
		if(PQ[i] == 99)
		{
			offset3 = i;
			break;
		}
	}
	/*
	for(i = 0; i < In; i++)
	{
		for(j=0; j < In; j++)
		{
			printf("%.8f ", PA[i+j*In+offset1]);
		}
		printf("\n");
	}
	printf("\n\n");
	
	for(i=0; i < In; i++)
	{
		printf("%.8f\n", Pw[i+offset2]);
	}
	*/
	double *fullMatrix = PA + offset1;
	double *eigenval = Pw + offset2;
	double *eigenvec = PQ + offset3;
	if(Icores <=16)
	{
		status = PLASMA_Init(Icores);
		printf("%d\n",status);
		status = PLASMA_Disable(PLASMA_AUTOTUNING);
		printf("%d\n",status);
		status = PLASMA_Set(PLASMA_TILE_SIZE, INB);
		printf("%d\n", status);
		status = PLASMA_Set(PLASMA_INNER_BLOCK_SIZE, IIB);
		printf("%d\n",status);
		PLASMA_desc *descT;
		status = PLASMA_Alloc_Workspace_dsyev(In,In,&descT);
		printf("%d\n",status);
		status = PLASMA_dsyev(PlasmaVec, PlasmaLower, In, fullMatrix, In, eigenval, descT, eigenvec, In);
		printf("%d\n",status);
		status = PLASMA_Finalize();
		printf("%d\n",status);
		PA = fullMatrix;
		Pw = eigenval;
		PQ = eigenvec;
		return(R_NilValue);
	}
	else
	{
		return(R_NilValue);
	}
}
