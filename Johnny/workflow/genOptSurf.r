library(fields)

#This script generates the data necessary to create the likelihood surface over which mKrig or Krig must optimize:

#these parameters define the domain over which the likelihood surface is optimized: 
lambda.lower = 0
lambda.upper = 2
theta.lower = 25
theta.upper = 10000

#Make a sampling grid:
lambdas = seq(lambda.lower, lambda.upper, length=200)
thetas = seq(theta.lower, theta.upper, length=100)

#get the data:
data(CO2)
lim = 20
limIter = 20
maxSize = 7000

#####Calculate likelihood surface over lambda and theta:
i=1
lims = c()
ns = c()
negLogLikes = list()
while(T) {
  #only use a subset of the data that is close enough to 0 latitude and 0 longitude:
  ind <- (-lim < CO2$lon.lat[,1]) & (CO2$lon.lat[,1] < lim) # only include pts with lon coords between -Xlim and Xlim
  ind <- (ind & -lim/2 < CO2$lon.lat[,2]) & (CO2$lon.lat[,2] < lim/2) # only include pts with lat coords between -Xlim/2 and Xlim/2
  
  lonlat = CO2$lon.lat[ind,]
  y <- CO2$y[ind]

  #if there's the same number of points as last time, continue:
  if(i > 1 && length(y) == ns[i - 1]) {
    lim = lim + limIter
    next
  } else if(length(y) > maxSize || lim > 180) {
    # if there are too many points in the matrix or all the points on the earth are covered, break:
    break
  }

  # else, record likelihoods:
  ns[i] = length(y)
  lims[i] = lim
  print(paste0('Using ', length(y), ' data points with lim: ', lim))
  
  #Calculate the likelihood values:
  negLogLikes[[i]] = matrix(nrow=length(thetas), ncol=length(lambdas))
  for(j in 1:length(thetas)) {
    krig = Krig(lonlat, y, lambda=1, theta=thetas[j], cov.args= list(Covariance="Exponential", Distance="rdist.earth"), GCV=TRUE)
    gcv = gcv.Krig(krig, lambda.grid=lambdas)
    negLogLikes[[i]][j,] = gcv$gcv.grid$'-lnLike Prof'
  }
  
  # iterate:
  lim = lim + limIter
  i = i + 1
}

#save data:
save(list=c("ns", "negLogLikes", "lims", "lambdas", "thetas"), file="~/code/R/workflow/likeSurf.RData")
