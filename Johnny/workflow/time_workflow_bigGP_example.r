### This demo is the example from our arXiv preprint on kriging in the SN2011fe dataset
### To do the full size version of this example, you need to set doSmallExample to FALSE
### To do the full likelihood optimization, set doOptim to TRUE
library(Rmpi)
library(fields)
require(bigGP, lib.loc='/glade/u/home/jpaige/code/')

workflow = function(n, probName, doInit) {
  SN2011fe_subset <- SN2011fe[1:n,]
  SN2011fe_newdata_subset <- SN2011fe_newdata[1:(80*80),]
  SN2011fe_mle <- SN2011fe_mle_subset
  nProc = 15 
  
  n <- nrow(SN2011fe_subset)
  m <- nrow(SN2011fe_newdata_subset)
  r <- 5
  
  nu <- 2
  
  print(n)
  print(m)
  
  inputs <- c(as.list(SN2011fe_subset), as.list(SN2011fe_newdata_subset), nu = nu)
  
  h_n = NULL 
  h_m = NULL 
  
  print('Constructing new KrigeProblem')
  prob <- krigeProblem$new(probName, h_n = h_n, numProcesses = nProc, n = n, m = m, h_m = h_m, predMeanFunction = SN2011fe_predmeanfunc, crossCovFunction = SN2011fe_crosscovfunc,  predCovFunction = SN2011fe_predcovfunc, meanFunction = SN2011fe_meanfunc, covFunction = SN2011fe_covfunc,  inputs = inputs, params = SN2011fe_mle$par, data = SN2011fe_subset$flux, packages = 'fields', doInit=doInit)
  
  print('Calculating log density')
  prob$calcLogDens()
  
  print('Setting parameters')
  prob$setParams(SN2011fe_initialParams)
  
  print('Optimizing log density')
  prob$optimizeLogDens(method = "L-BFGS-B", verbose = TRUE, lower = rep(.Machine$double.eps, length(SN2011fe_initialParams)), control = list(parscale = SN2011fe_initialParams))
  
  print('Predicting')
  pred <- prob$predict(ret = TRUE, se.fit = TRUE, verbose = TRUE)
  
  print('Simulating realizations')
  realiz <- prob$simulateRealizations(r = r, post = TRUE, verbose = TRUE)
  
  invisible(NULL)
}

minSize = 4000
maxSize = 20000
nIter = 100
ns = seq(minSize, maxSize, length=nIter)
times = ns
for(i in 1:nIter) {
  #set up input variables
  n = ns[i]
  doInit = (i == 1)
  probName = paste0('prob', i)
  
  print(paste('n:', n))
  
  #time workflow:
  times[i] = system.time(workflow(n, probName, doInit))[3]
}

save(list=c("ns", "times"), file=paste0("~/code/R/workflow/bigGPWorkflowTimes_n", maxSize, "_p", nProc, ".RData"))
