library(fields)
require('RandomFields', lib.loc='/glade/u/home/jpaige/code/')
require('geoR', lib.loc='/glade/u/home/jpaige/code/')

#The following script computes how long the Krig and mKrig takes for many sized matrices
#using the CO2 dataset from fields.  Uses arc distances.

workflow = function(lonlat, y) {
  #time how long it takes for geoR to generate a Kriging surface with error estimates
  
  #Do ML estimation on covariance params:
  nugget.init = .25 #initial estimate for nugget
  ini.cov.pars=c(.4, 15) # initial estimates for partial sill and range parameters
  parLims = pars.limits(phi=c(0, 50), sigmasq=c(0, 5)) #optimization domain (what are nugget.rel and tausq.rel?)
  like = likfit(coords=lonlat, data=y, trend='1st', ini.cov.pars=ini.cov.pars,
                nugget=nugget.init, cov.model='exponential', nospatial=FALSE,
                limits=parLims)
  
  ####Calculate Kriging suface using universal Kriging (ordinary kriging plus 1st order trend):
  #get Kriging surface:
  krigeC = krige.control(trend.d='1st', trend.l='1st', obj.model=like,
                     cov.model='exponential', cov.pars=like$cov.pars,
                     nugget=like$nugget)
  
  #generate prediction locations on 80 by 80 grid:
  minLon = min(lonlat[,1])
  maxLon = max(lonlat[,1])
  minLat = min(lonlat[,2])
  maxLat = max(lonlat[,2])
  lons = rep(seq(minLon, maxLon, length=80), length=80*80)
  lats = rep(seq(minLat, maxLat, length=80), each=80)
  predLocs = cbind(lons, lats)
  
  #make all prediction locations be in convex hull of data:
  predLocs = predLocs[in.poly(predLocs, lonlat, convex.hull=TRUE),]
  
  #Predict data (locations are prediction locations):
  krige = krige.conv(coords=lonlat, data=y, locations=predLocs, krige=krigeC)
  
  #plotting data:
  #quilt.plot(predLocs, krige$predict)
  #quilt.plot(predLocs, krige$krige.var)
  
  invisible(NULL)
}

#now time the workflow for Krig or mKrig using CO2 dataset from fields:
data(CO2)

maxSize = 8000 # maximum matrix size (number of observations) for timing
limIter = 10   # how far to increase lat/lon limits on CO2 data per iteration
lim <- 15     # starting limit for how many degrees lat/lon points can be from each other
i = 1

lims = c()
ns = c()
times = c()
while(T) {
  #only use a subset of the data that is close enough to 0 latitude and 0 longitude:
  ind <- (-lim < CO2$lon.lat[,1]) & (CO2$lon.lat[,1] < lim) # only include pts with lon coords between -Xlim and Xlim
  ind <- (ind & -lim/2 < CO2$lon.lat[,2]) & (CO2$lon.lat[,2] < lim/2) # only include pts with lat coords between -Xlim/2 and Xlim/2
  
  lonlat = CO2$lon.lat[ind,]
  y <- CO2$y[ind]
  
  print(paste0('Using ', length(y), ' data points with lim: ', lim))
  
  #if there's the same number of points as last time, continue:
  if(i > 1 && length(y) == ns[i - 1]) {
    lim = lim + limIter
    next
  } else if(length(y) > maxSize || lim > 180) {
    # if there are too many points in the matrix or all the points on the earth are covered, break:
    break
  }
  
  # else, record timings:
  ns[i] = length(y)
  lims[i] = lim
  
  times[i] = system.time(workflow(lonlat, y))[3]
  
  # iterate:
  lim = lim + limIter
  i = i + 1
}

#save data:
save(list=c("ns", "times", "lims"), file=paste0("~/code/R/workflow/geoRWorkflowTimes.RData"))
