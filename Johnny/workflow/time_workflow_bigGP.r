library(fields)
library(Rmpi)
require(bigGP, lib.loc='/glade/u/home/jpaige/code/')

#The following script computes how long the bigGP workflow takes for many sized matrices
#using the CO2 dataset from fields.  Uses arc distances.

#set number of cores:
D = 5
nSlaves = D*(D+1)/2

#Use these mean and covariance functions for bigGP:
#input functions to initialize krigeProblem:
covFunction = function(params, inputs, indices=T, cached=NULL) {
  vnorm = function(v) {sqrt(sum(abs(v)^2))}
  dists = apply(matrix(inputs$datCoords[indices[,1],] - inputs$datCoords[indices[,2],], ncol=2), 1, vnorm)
   
  #calculate covariance using fields' Exponential(d, range = 1, alpha = 1/range, phi = 1)
  out = Exponential(dists, range=params[1])
  if(sum(out < 0) > 0) {
    save(inputs, indices, file='negCov.RData')
    stop('Theres an error')
  }
  return(out)
}
crossCovFunction = function(params, inputs, indices=T, cached=NULL) {
  vnorm = function(v) {sqrt(sum(abs(v)^2))}
  dists = apply(matrix(inputs$datCoords[indices[,1],] - inputs$predCoords[indices[,2],], ncol=2), 1, vnorm)

  #calculate covariance using fields' Exponential(d, range = 1, alpha = 1/range, phi = 1)
  out = Exponential(dists, range=params[1])
  if(sum(out < 0) > 0) {
    save(inputs, indices, file='negCov.RData')
    stop('Theres an error')
  }
  return(out)
}
predCovFunction = function(params, inputs, indices=T, cached=NULL) {
  vnorm = function(v) {sqrt(sum(abs(v)^2))}
  dists = apply(matrix(inputs$predCoords[indices[,1],] - inputs$predCoords[indices[,2],], ncol=2), 1, vnorm)

  #calculate covariance using fields' Exponential(d, range = 1, alpha = 1/range, phi = 1)
  out = Exponential(dists, range=params[1])
  if(sum(out < 0) > 0) {
    save(inputs, indices, file='negCov.RData')
    stop('Theres an error')
  }
  return(out)
}
meanFunction = function(params, inputs, indices=T, cached=NULL) {
  out = try(inputs$datCoords[indices,1]*0 + 375.8304)
  if(class(out) == 'try-error') {
    save(inputs, indices, file='meanError.RData')
    stop('this makes no sense')
  } else {
    return(out)
  }
} #based on mean(CO2$y)
predMeanFunction = function(params, inputs, indices=T, cached=NULL) {
  out = try(inputs$predCoords[indices,1]*0 + 375.8304)
  if(class(out) == 'try-error') {
    save(inputs, indices, file='meanError.RData')
    stop('this makes no sense')
  } else {
    return(out)
  }
}

workflow = function(lonlat, y, probName, doInit) {
  #timeKrig tells whether or not to time Krig or mKrig lambda optimization procedure.
  #If TRUE, times Krig, else mKrig
  
  #generate prediction locations on 80 by 80 grid:
  minLon = min(lonlat[,1])
  maxLon = max(lonlat[,1])
  minLat = min(lonlat[,2])
  maxLat = max(lonlat[,2])
  lons = rep(seq(minLon, maxLon, length=80), length=80*80)
  lats = rep(seq(minLat, maxLat, length=80), each=80)
  predLocs = cbind(lons, lats)
  
  #make all prediction locations be in convex hull of data:
  predLocs = predLocs[in.poly(predLocs, lonlat, convex.hull=TRUE),]
  predLocs = list(predCoords=predLocs)
  lonlat = list(datCoords=lonlat)
  
  #create new krigeProblem:
  print('Creating krigeProblem')
  params=5 #this is the theta parameter for the exponential covariance function in fields
  prob <- krigeProblem$new(probName, h_n=NULL, numProcesses=nSlaves,
                            n=nrow(lonlat$datCoords), m=nrow(predLocs$predCoords), h_m=NULL, predMeanFunction = predMeanFunction,
                            crossCovFunction=crossCovFunction,
                            predCovFunction=predCovFunction, meanFunction=meanFunction,
                            covFunction=covFunction, inputs=c(lonlat, predLocs), params=params, data=y,
                            packages='fields', doInit = doInit)
  
  #calculate log-density of the data: TODO: Is this necessary?
  print('Calculating log density')
  prob$calcLogDens()
  
  #perform parameter optimization
  print('Optimizing log density')
  out = try(prob$optimizeLogDens(method = "L-BFGS-B", verbose = TRUE, lower=.Machine$double.eps, control=list(parscale=params)))
  if(class(out) == 'try-error') {
    C <- collectTriangularMatrix('C', objPos =probName, n = prob$n, h = prob$h_n)
    save(C, file='covMat.RData')
    stop('this makes no sense')
  }
  
  print(out)
  
  #set parameters to MLE:
  print('Setting parameters to MLE')
  prob$setParams(out$par)
  
  #Calculate predictions
  print('Predicting')
  pred <- prob$predict(ret = TRUE, se.fit = TRUE, verbose = TRUE)
  print('Simulating realizations')
  r=100
  realiz <- try(prob$simulateRealizations(r = r, post = TRUE, verbose = TRUE))
  if(class(realiz) == 'try-error') {
    C <- collectTriangularMatrix('postC', objPos =probName, n = prob$m, h = prob$h_m)
    save(C, file='covMat.RData')
    stop('this makes no sense')
  }
  
  invisible(NULL)
}

#now time the workflow for Krig or mKrig using CO2 dataset from fields:
data(CO2)

maxSize = 10000 # maximum matrix size (number of observations) for timing
limIter = 1   # how far to increase lat/lon limits on CO2 data per iteration
startLim = 70 # starting limit for how many degrees lat/lon points can be from each other
lim <- startLim

#print out help files:
krigeProblem$help('calcLogDens')
krigeProblem$help('optimizeLogDens')
krigeProblem$help('predict')
krigeProblem$help('simulateRealizations')

lims = c()
ns = c()
times = c()
i = 1
while(T) {
  #only use a subset of the data that is close enough to 0 latitude and 0 longitude:
  ind <- (-lim < CO2$lon.lat[,1]) & (CO2$lon.lat[,1] < lim) # only include pts with lon coords between -Xlim and Xlim
  ind <- (ind & -lim/2 < CO2$lon.lat[,2]) & (CO2$lon.lat[,2] < lim/2) # only include pts with lat coords between -Xlim/2 and Xlim/2
  
  #get data subset latitude and longitude locations as well as the observed values at the locations:
  lonlat = CO2$lon.lat[ind,]
  y <- CO2$y[ind]
  
  #if there's the same number of points as last time, continue:
  if(i > 1 && length(y) == ns[i - 1]) {
    lim = lim + limIter
    next
  } else if(length(y) > maxSize || lim > 180) {
    # if there are too many points in the matrix or all the points on the earth are covered, break:
    break
  }
  
  # else, record timings:
  ns[i] = length(y)
  lims[i] = lim
  
  print(paste0('Using ', ns[i], ' points'))
  
  #only initialize for first iteration, have a different problem name each iteration:
  doInit = lim == startLim
  probName = paste0('problem', lim)
  
  #time the bigGP workflow:
  times[i] = system.time(workflow(lonlat, y, probName, doInit))[3]
  
  # iterate:
  lim = lim + limIter
  i = i + 1
}

#save data:
save(list=c("ns", "times", "lims"), file=paste0("~/code/R/workflow/bigGPWorkflowTimes_n", maxSize, "_D", D, ".RData"))
