library(fields)
library(Rmpi)

#This script calculates the time required to compute many default Cholesky decompositions
#in parallel (for 12259 data points)

# In case R unexpectedly quits, clean up Rmpi processes and memory
.Last <- function(){
  if (is.loaded("mpi_initialize")){
    if (mpi.comm.size(0) > 0){
      print("Use mpi.close.Rslaves() to close slaves.")
      mpi.close.Rslaves()
    }
    print("Use mpi.quit() to quit R")
    .Call("mpi_finalize")
  }
}

#All processors calculate covariance matrix (Sigma) and its Cholesky decomp
processorTask = function(x) {
  return(chol(Sigma))
}

##### set up parallel problem:
nTimes = 2

#get data:
data(CO2)
#lim <- 119 #limit for how many degrees lat/lon points can be from each other
maxSize = 5000 # maximum matrix size (number of observations) for timing
limIter = 3   # how far to increase lat/lon limits on CO2 data per iteration
lim <- 15     # starting limit for how many degrees lat/lon points can be from each other
i = 1

parChol = function() {
#This function sends the necessary data to the slaves and collects the calculated cholesky decomp

#spawn slaves:
nslaves = 1
commTime = 0
commTime = commTime + system.time(mpi.spawn.Rslaves(nslaves = nslaves, needlog=F))[3]

#send coordinates, rdist.earth, and task function to slaves (don't bcast because in general not same matrix):
commTime = commTime + system.time(mpi.bcast.Robj2slave(Sigma))[3]
commTime = commTime + system.time(mpi.bcast.Robj2slave(processorTask))[3]

#run parallel computation:
procTime <- system.time(Ls <- mpi.iapplyLB(1:nTimes, processorTask))[3]

#end:
commTime = commTime + system.time(mpi.close.Rslaves())[3]
return(list(commTime = commTime, procTime = procTime)) #NOTE: Ls should not be returned unless debugging
}

lims = c()
ns = c()
totTimes = c()
commTimes = c()
procTimes= c()
while(T) {
  #only use a subset of the data that is close enough to 0 latitude and 0 longitude:
  ind <- (-lim < CO2$lon.lat[,1]) & (CO2$lon.lat[,1] < lim) # only include pts with lon coords between -Xlim and Xlim
  ind <- (ind & -lim/2 < CO2$lon.lat[,2]) & (CO2$lon.lat[,2] < lim/2) # only include pts with lat coords between -Xlim/2 and Xlim/2
  
  lonlat = CO2$lon.lat[ind,]
   
  print(paste0('Using ', nrow(lonlat), ' data points with lim: ', lim))
  
  #if there's the same number of points as last time, continue:
  if(i > 1 && nrow(lonlat) == ns[i - 1]) {
    lim = lim + limIter
    next
  } else if(nrow(lonlat) > maxSize || lim > 180) {
    # if there are too many points in the matrix or all the points on the earth are covered, break:
    break
  }
  
  #make covariance matrix:
  theta = 4000 #exponential range parameter in miles
  distanceMatrix<- rdist.earth(lonlat, lonlat)
  # an exponential covariance function
  Sigma<- exp(-distanceMatrix/theta)
  
  # else, record timings:
  ns[i] = nrow(lonlat)
  lims[i] = lim
  totTimes[i] = system.time(out <- parChol())[3]
  procTimes[i] = out$procTime
  commTimes[i] = out$commTime
  
  #clear memory on slaves: (slaves already closed, not necessary)
  #mpi.bcast.cmd(rm(list=c('Sigma', 'processorTask')))
  
  # iterate:
  lim = lim + limIter
  i = i + 1
}

save(list=c('ns', 'lims', 'totTimes', 'procTimes', 'commTimes'), file=paste0("~/code/R/Rmpi/time_parChol_n", maxSize, "_test.RData"))

#Ls = out$Ls
#for(i in 1:length(Ls)) {\
#  L = Ls[[i]]
#  if(class(L) != 'matrix') {
#    print(i)
#    print(L)
#  }
#}

mpi.quit()
