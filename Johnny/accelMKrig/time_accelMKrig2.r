library(fields)
source('myMKrig.family.r')

# convert latitude and longitude to 3D ECEF coordinates assuming all points are at sea-level:
LLA_to_ECEF = function(lat, lon, convertToRad=F) {
  # sources:
  # http://www.mathworks.de/help/toolbox/aeroblks/llatoecefposition.html
  # http://stackoverflow.com/questions/10473852/convert-latitude-and-longitude-to-point-in-3d-space
  
  f  = 1.0/298.257223563             # flattening to account for Earth as oblate spheroid
  ls = atan((1 - f)^2 * tan(lat))    # lambda, the mean sea-level
  R = 6378137                        # Equatorial radius of Earth (in meters)
  
  # Convert lat and lon to radians:
  if(convertToRad) {
    lat = pi/180*lat
    lon = pi/180*lon
  }
  
  #assumes all points are at sea-level:
  cosLat = cos(lat)
  sinLat = sin(lat)
  
  rad = R/sqrt(1 + (1/(1 - f)^2 - 1)*sin(ls)^2) # mean sea-level radius
  
  
  x = rad*cosLat * cos(lon)
  y = rad*cosLat * sin(lon)
  z = rad*sinLat
  
  return(cbind(x, y, z))
}

accel_mKrig_time = function() {
  #The following script computes how long the Krig and mKrig takes for many sized matrices
  #using the CO2 dataset from fields.  Uses chordal rather than arc distances.
  
  data(CO2)
  
  #convert coorindates to 3D, ECEF coordinates:
  lonLat = CO2$lon.lat
  #ecef = LLA_to_ECEF(lonLat[,1], lonLat[,2])
  
  maxSize <<- 20000 # maximum matrix size for timing
  limIter = 3   # how far to increase lat/lon limits on CO2 data per iteration
  lim <- 15     # starting limit for how many degrees lat/lon points can be from each other
  i = 1
  
  lims <- c()
  ns <- c()
  times <- c()
  while(T) {
    ind <- (-lim < CO2$lon.lat[,1]) & (CO2$lon.lat[,1] < lim) # only include pts with lon coords between -Xlim and Xlim
    ind <- (ind & -lim/2 < CO2$lon.lat[,2]) & (CO2$lon.lat[,2] < lim/2) # only include pts with lat coords between -Xlim/2 and Xlim/2
    
    x <- lonLat[ind,]
    y <- CO2$y[ind]
    
    #if there's the same number of points as last time, continue:
    if(i > 1 && length(y) == ns[i - 1]) {
      lim = lim + limIter
      next
    }
    # if there are too many points in the matrix or all the points on the earth are covered, break:
    else if(length(y) > maxSize || lim > 180) {
      break
    }
    
    # else, record timings:
    ns[i] = length(y)
    lims[i] = lim
    
    print(paste0('n: ', ns[i], ', lim: ', lims[i]))
     
    times[i] = system.time(obj<- myMKrig(x, y, theta=8, lambda=.1,
                                       cov.args= list(Covariance="Exponential", Distance="rdist"),
                                       MAGMA_n = 2))[3]
    
    # iterate:
    lim = lim + limIter
    i = i + 1
  }
   
  #save data:
  print(lims)
  print(ns)
  print(times)
  return(list(ns=ns, times=times, lims=lims))
}

out = accel_mKrig_time()
ns = out$ns
lims = out$lims
times = out$times
save(list=c("ns", "times", "lims"), file=paste0("~/code/R/accelMKrig/accelMKrigTimes2_n", maxSize, ".RData"))
