library(fields)
source('/picnic/u/home/jpaige/code/R/Rwrapper_MAGMA/Rwrapper_MAGMA.r')

#This script uses the CO2 dataset from frields to calculate the cholesky decmpositon times
#from the defualt cholesky decompostion function (chol)

theta <- 4000
genMatrix = function() {
  # matrix of pairwise distances
  distanceMatrix <- rdist.earth(lonlat, lonlat)
  # an exponential covariance function
  Sigma <- exp(-distanceMatrix/theta)
  return(Sigma)
}

data(CO2)

maxSize = 15000 # maximum matrix size (number of observations) for timing
limIter = 3   # how far to increase lat/lon limits on CO2 data per iteration
lim <- 15     # starting limit for how many degrees lat/lon points can be from each other
i = 1

lims = c()
ns = c()
times = c()
cTimes = c()
while(T) {
  #only use a subset of the data that is close enough to 0 latitude and 0 longitude:
  ind <- (-lim < CO2$lon.lat[,1]) & (CO2$lon.lat[,1] < lim) # only include pts with lon coords between -Xlim and Xlim
  ind <- (ind & -lim/2 < CO2$lon.lat[,2]) & (CO2$lon.lat[,2] < lim/2) # only include pts with lat coords between -Xlim/2 and Xlim/2
  
  lonlat <- CO2$lon.lat[ind,]
  
  #if there's the same number of points as last time, continue:
  if(i > 1 && nrow(lonlat) == ns[i - 1]) {
    lim = lim + limIter
    next
  } else if(nrow(lonlat) > maxSize || lim > 180) {
    # if there are too many points in the matrix or all the points on the earth are covered, break:
    break
  }
  
  print(paste0('Using ', nrow(lonlat), ' data points with lim: ', lim))
  
  # else, record timings:
  ns[i] = nrow(lonlat)
  lims[i] = lim
  
  Sigma = genMatrix()
  times[i] = system.time(L <- magmaChol_2gpu(Sigma))[3]
  
  # iterate:
  lim = lim + limIter
  i = i + 1
}

#save data:
save(list=c("ns", "times", "cTimes", "lims"), file=paste0("~/code/R/Rwrapper_MAGMA/timeWrapperChol2_n", maxSize, ".RData"))
