dyn.load("/picnic/u/home/jpaige/code/R/r-timing-code/magmaTiming/magmaCholesky3.so")
dyn.load("/picnic/u/home/jpaige/code/R/r-timing-code/magmaMGPU/magmaCholesky_mgpu.so")

magmaChol_1gpu = function(A, id){
  #Function that checks for GPU
  if(!is.matrix(A)){
    stop("Input must be a matrix")
  }
  n = dim(A)
  if(n[1] != n[2]){
    stop("Matrix is not square")
  }
  if(n[1]>26000){
    stop("Matrix too large: Will not fit in GPU memory")
  }
  
  #Force A to be deep copied:
  A[1] = A[1]
  
  #nb parameter not used at this point but needed for function call
  nb = 0
  .Call("magmaCholesky",A,n[1],nb, id)
  return(A)
}

magmaChol_2gpu = function(A){
  #Function that checks for GPU
  if(!is.matrix(A)){
    stop("Input must be a matrix")
  }
  n = dim(A)
  if(n[1] != n[2]){
    stop("Matrix is not square")
  }
  if(n[1]>35000){
    stop("Matrix too large: Will not fit in GPU memory")
  }

  #Force A to be deep copied:
  A[1] = A[1]
  
  #nb paramter not used at this point but needed for function call
  nb = 0
  .Call("magmaCholeskyMGPU",A,n[1],nb)
  return(A)
}
