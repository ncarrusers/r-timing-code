#install.packages('fields', lib='/glade/u/home/jpaige/code/', repos='http://cran.rstudio.com/')
require('spam', lib.loc='/glade/u/home/jpaige/code/')
require('maps', lib.loc='/glade/u/home/jpaige/code/')
require('fields', lib.loc='/glade/u/home/jpaige/code/')
source("http://dl.dropboxusercontent.com/u/27443652/365/365Functions.r")

#This function generates observations based on covariance matrix C
genObservations = function(C) {
  U = chol(C)
  L = t(U)
  n = nrow(C)
  x = rnorm(n)
  return(L %*% x)
}

#returns an n x n covariance matrix using points distributed with uniform
#distribution on [0,1], [0,1] square.
genUnif = function(n) {
  x<- matrix(runif(n*2), ncol=2, nrow=n)
  return(x)
}

#returns an n x n covariance matrix using points distributed in even grid
#on [0,1], [0,1] square.
genGrid = function(n) {
  M<- round(sqrt(n))
  xgrid<- seq(0, 1, length=M)
  ygrid<- xgrid
  
  x<- make.surface.grid( list(x= xgrid, y=ygrid))
  return(x)
}

#returns covariances matrix using exponential covariance function based
#on input pts in n x 2 array
genCovMat = function(x) {
  theta<- .3
  # matrix of pairwise distances
  distanceMatrix<- rdist(x, x)
  # an exponential covariance function
  Sigma<- exp(-distanceMatrix/theta)
  return(Sigma)
}

#This function times how long mKrig takes for an n x n covariance matrix
#Assumes uniformly distributed data and a scale parameter of .3 for an exponential covariance model
timer2 = function(n) {
  # NOTE if n gets too large the Cholesky may fail 
  # generate pts randomly distributed on [0,1], [0,1] square
  x<- genUnif(n)
  # generate exponential covariance matrix:
  Sigma <- genCovMat(x)
  # compute random observations at each pt based on covariance matrix:
  y = genObservations(Sigma)
  print(dim(Sigma))
  # return how long mKrig takes
  return(system.time(mKrig(x, y), gcFirst=T))
}

makeGridPoints = function(x, y) {
  xCoords = rep(x, M)
  
  Y = matrix(y, nrow=M, ncol=M)
  yCoords = as.vector(t(Y))
  return(cbind(xCoords, yCoords))
}

#This function times how long mKrig takes for an n x n covariance matrix
#Assumes data is in grid and a scale parameter of .3 for an exponential covariance model
timer3 = function(n) {
  # NOTE if n gets too large the Cholesky may fail 
  
  # generate pts in even grid on [0,1], [0,1] square
  x<- genGrid(n)
  # generate exponential covariance matrix:
  Sigma <- genCovMat(x)
  # compute random observations at each pt based on covariance matrix:
  y = genObservations(Sigma)
  print(dim(Sigma))
  # return how long mKrig takes
  return(system.time(mKrig(x, y), gcFirst=T))
}

myrdist = function(x) {
  n = nrow(x)
  D = matrix(nrow=n, ncol=n)
  
  for(i in 1:n) {
    if(i == n) {
      D[i,i] = 0
      break
    }
    
    diffVecs = t(x[i:n,]) - x[i,]
    dists = apply(diffVecs, 2, vnorm)
    
    D[i,i:n] = dists
    D[i:n,i] = dists
  }
  
  return(D)
}

mKrig_time_r = function(timerID) {
  
  #The following script computes how long mKrig takes for many sized matrices
  nPts = 100
  maxSize = 20000
  
  # NOTE: mKrig seems to fail for fewer than 3 data points, so start at 3:
  ns = round(seq(3, maxSize, length=nPts))
  times = ns
  for(i in 1:length(ns)) {
    n = ns[i]
    if(timerID == 3) {
      out = timer3(n)
    }
    else {
      out = timer2(n)
    }
    times[i] = out[3]
  }
  if(timerID == 3) {
    #in timer3, the true n values are "nearest" square numbers due to rounding and grid setup:
    Ms = round(sqrt(ns))
    ns = Ms^2
    
    #now average all times that were rounded to the same n:
    uniqueNs = unique(ns)
    uniqueTimes = uniqueNs
    for(i in 1:length(uniqueNs)) {
      n = uniqueNs[i]
      indices = which(ns %in% n)
      uniqueTimes[i] = mean(times[indices])
    }
    ns = uniqueNs
    times = uniqueTimes
  }
  lNs = log(ns)
  
  lTimes = log(times)
  firstLTimes = lTimes[1:(length(lTimes)-1)]
  lastLTimes = lTimes[2:length(lTimes)]
  diffLTimes = lastLTimes - firstLTimes
  
  firstNs = lNs[1:(length(lNs) - 1)]
  lastNs = lNs[2:length(lNs)]
  diffLNs = lastNs - firstNs
  slope = diffLTimes/diffLNs
  
  if(timerID == 3)
    save(list=c("ns", "times", "slope"), file="~/code/R/timer3_mKrig.RData")
    #save(list=c("ns", "times", "slope"), file="~/NCAR/timer3_mKrig.RData")
  else
    save(list=c("ns", "times", "slope"), file="~/code/R/timer2_mKrig.RData")
    #save(list=c("ns", "times", "slope"), file="~/NCAR/timer2_mKrig.RData")
}

#pdf("blah.pdf", height=5, width=5)
#plot(ns, times, main="Default Cholesky Decomposition Time", xlab="Matrix Dimension", ylab="Time (Seconds)", type='l', col='red')
#dev.off()
#plot(log(ns), log(times), ylim=c(-7, 7), main="Log Default Cholesky Decomposition Time", xlab="Log Matrix Dimension", ylab="Log Time (Log Seconds)")
#points(log(ns[2:length(ns)]), slope, col='red')
#sums = cumsum(slope[7:length(slope)])
#means = sums/1:length(sums)
#plot(ns[1:length(sums) + 7], means, main="Slope Moving Average", ylab="Slope", xlab="Matrix Dimension", type='l', col='red', lwd=2.2)
#abline(a=3, b=0, col='green', lwd=2.2)

#plot(log(ns), log(times), ylim=c(-7, 7), main="Log Default Cholesky Decomposition Time", xlab="Log Matrix Dimension", ylab="Log Time (Log Seconds)", type='l', lwd=2.2)
#points(log(ns[2:length(ns)]), slope, col='red', bg='red', cex=.3, pch=21)
#abline(a=3, b=0, col='green', lwd=2.2)
#legend('topleft', c('Log Time', 'Obsevered Slope', 'Theoretical Slope'), col=c('black', NA, 'green'), lty=1:2, lwd=2.2, pch=c(NA, NA))
#legend('topleft', c('','',''), col='red', pt.cex=.3, lty=c(0,0), pch=c(NA,21,NA), pt.bg='red', bty='n')

cholesky_variance_r = function(timerID) {
  nIters = 100
  n = round(sqrt(5000))^2
  times = vector(mode="double", length=100)
  
  for(i in 1:nIters) {
    if(timerID == 2)
      times[i] = timer2(n)[1]
    else if(timerID == 3)
      times[i] = timer3(n)[1]
    else
      times[i] = timer1(n)[1]
  }
  
  if(timerID == 2)
    save(list=c("n", "times"), file="~/code/R/variance2.RData")
  else if(timerID == 3)
    save(list=c("n", "times"), file="~/code/R/variance3.RData")
  else
    save(list=c("n", "times"), file="~/code/R/variance1.RData")
}

#boxplot(times, main="Cholesky Decomposition Times for n=5000", ylab="Time (sec)")
#hist(times, main="Cholesky Decomposition Time, Random Points (n=5000)", breaks=seq(2.38, 2.52, length=12), freq=F, xlim=c(2.38, 2.52), xlab="Times (sec)", col=rgb(0,0,1, .3))
