library(Rmpi)
library(fields)
require(bigGP, lib.loc='/glade/u/home/jpaige/code/')

#get command line arguments:
args <- commandArgs(trailingOnly = TRUE)
D = as.numeric(args[2])
print(args)
print(D)

#This function estimates the optimal block replication factor for Cholesky decomposition:
calcH = function(n, D){
  'Description:
Calculates a good choice of the block replication factor given the number of values.

n: numeric. The number of values being subdivided amongst the processes and blocks.
D: the number of blocks per length of the matrix

Value:
numeric. The value of H, the block replication factor.'
  
  return(max(n %/% (D * 1000), 1))
}

#input functions to initialize krigeProblem:
predMeanFunction = function(params, inputs, indices=T, cached=NULL) {1}
crossCovFunction = function(params, inputs, indices=T, cached=NULL) {1}
predCovFunction = function(params, inputs, indices=T, cached=NULL) {1}
meanFunction = function(params, inputs, indices=T, cached=NULL) {0}
covFunction = function(params, inputs, indices=T, cached=NULL) {
  vnorm = function(v,p=2) {
    if ( p =="I")
      return(max(abs(v)))
    else
      return(sum(abs(v)^p)^(1/p))
  }
  
  if(is.null(cached))
    cachedIsNull = TRUE else cachedIsNull = FALSE
  if(cachedIsNull){
    cached = list()
    
    cached$dists <- apply(matrix(inputs$coords[indices[,1],] - inputs$coords[indices[,2],], ncol=2), 1, vnorm)
  }
  dists <- cached$dists
  
  #calculate covariance using fields' Exponential(d, range = 1, alpha = 1/range, phi = 1)
  return(Exponential(dists, alpha=params[1]))
}

#Function used to calculate Cholesky decomposition with bigGP
#size: number of observations, AKA matrix dimension
#nslaves: number of processes with which to calculate Cholesky decomp
#D: number of processor blocks per length of matrix
chol_bigGP = function() {
  print(paste0('size: ', size))
  
  #calculate Cholesky decomposition:
  print('Calculating Cholesky decomposition')
  remoteCalcChol(matName='C', cholName='L', matPos='prob', cholPos='prob', n=prob$n, h=prob$h_n)
  
  #collect Cholesky Decomposition:
  print('Collecting Cholesky decomposition')
  L <- collectTriangularMatrix('L', objPos = 'prob', n = prob$n, h = prob$h_n)
  print('Finished collecting Cholesky decomposition')
  
  #return decomposition
  return(L)
}

#The following script computes how long the cholesky decomposition takes
#for different numbers of cores:

#intitialize loop variables:
nReps = 4
maxSize = 600
sizes = round(seq(1, maxSize, length=nReps))
nSlaves = D*(D+1)/2

times = double(nReps)
correctResults = times

#initialize new krigeProblem:
#generate observation locations:
x <- list(coords=matrix(runif(sizes[1]*2), ncol=2, nrow=sizes[1]))

print('Making new Krig problem')
prob <- krigeProblem$new('prob', h_n=NULL, numProcesses=nSlaves, 
                          n=sizes[1], m=0, h_m=NULL, predMeanFunction = predMeanFunction, 
                          crossCovFunction=crossCovFunction, 
                          predCovFunction=predCovFunction, meanFunction=meanFunction, 
                          covFunction=covFunction, inputs=x, params=.3, data=c(), 
                          packages='fields')

for(r in 1:nReps) {
  size = sizes[r] 
  
  #adjust observation locations and number of observations:
  x <- list(coords=matrix(runif(size*2), ncol=2, nrow=size))
  prob$inputs = x
  prob$n = size
  
  #reset block replication factor (prob$h_n):
  prob$h_n = calcH(size, D)
  
  print(paste('rep:', r, 'size:', size, 'prob$n:', prob$n, 'prob$h_n:', prob$h_n))
  
  #regenerate covariance matrix for new observation locations:
  print('Regenerating Covariance matrix')
  prob$remoteConstructCov()
  
  #testing generated Covariance matix:
  print('Collecting covariance matrix')
  C <- collectRectangularMatrix('C', objPos = 'prob', n1=prob$n, n2=prob$n, h1=prob$h_n, h2=prob$h_n)
  print('Finished collecting covariance matrix')
  print(C)
  
  #time bigGP Cholesky decomposition:
  times[r] = system.time(L <- chol_bigGP())[3]
  
  #check results with default Cholesky:
  L2 = t(chol(C))
  correctResults[r] = all.equal(L, L2, scale=1, tol=.05)
}

#save results and quit:
save(list=c("nSlaves", "times", 'correctResults'), file=paste("~/code/R/bigGP/bigGPTimes_n", 
                                                              maxSize, "_p", nSlaves + 1, ".RData", sep=''))
bigGP.quit(save = "no")

#Number of slaves must be D(D+1)/2 for some integer D:
#D = 2:floor((-1 + sqrt(1 + 8*maxSlaves))/2) #based on quadratic equation
#nSlaves = D*(D+1)/2
