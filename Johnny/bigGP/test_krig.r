.libPaths(c('/glade/u/home/jpaige/code', .libPaths()))
library(Rmpi)
require('spam', lib.loc='/glade/u/home/jpaige/code/')
require('maps', lib.loc='/glade/u/home/jpaige/code/')
require('fields', lib.loc='/glade/u/home/jpaige/code/')
require(bigGP, lib.loc='/glade/u/home/jpaige/code/')
source("http://dl.dropboxusercontent.com/u/27443652/365/365Functions.r")

chol_bigGP = function(A, npes) {
  bigGP.init(npes) #+++++++++++++++
  h <- 1 #+++++++++++++++
  print(ls())
  remoteCalcChol(matName='A', cholName='L', n=npes, h=h) #+++++++++++++++ NOTE: chol.args is not used here and pivoting is not implemented
  L <- collectTriangularMatrix('L', n = n, h = h) #+++++++++++++ NOTE: this is lower triangular, not upper triangular as returned by chol()
  bigGP.exit()
  bigGP.quit(save = "no")
  return(L)
}

genMatrix = function(n) {
  theta<- .3
  x <- matrix(runif(n*2), ncol=2, nrow=n)
  # matrix of pairwise distances
  distanceMatrix <- rdist(x, x)
  # an exponential covariance function
  Sigma<- exp(-distanceMatrix/theta )
  return(Sigma)
}

#The following script computes how long the cholesky decomposition takes for many sized matrices
size = 500
maxCores = 16
maxSlaves = maxCores - 1

predMeanFunction = function(params, inputs, indices=T, cached=NULL) {1}
crossCovFunction = function(params, inputs, indices=T, cached=NULL) {1}
predCovFunction = function(params, inputs, indices=T, cached=NULL) {1}
meanFunction = function(params, inputs, indices=T, cached=NULL) {0}
covFunction = function(params, inputs, indices=T, cached=NULL) {
  vnorm = function(v,p=2) { 
    if ( p =="I") {
      return(max(abs(v)))
    }
    else {
      return(sum(abs(v)^p)^(1/p))
    }
  }
  
  if(is.null(cached))
    cachedIsNull = TRUE else cachedIsNull = FALSE
  if(cachedIsNull){
    cached = list()
    
    #Exponential(d, range = 1, alpha = 1/range, phi = 1)
    cached$dists <- apply(inputs$coords[indices[,1],] - inputs$coords[indices[,2],], 1, vnorm)
  }
  dists <- cached$dists
  
  return(Exponential(dists, alpha=params[1]))
}

x <- list(coords=matrix(runif(size*2), ncol=2, nrow=size))

prob <- krigeProblem$new('prob', h_n=NULL, numProcesses=maxSlaves, n=size, m=0, h_m=NULL, predMeanFunction = predMeanFunction, crossCovFunction=crossCovFunction, predCovFunction=predCovFunction, meanFunction=meanFunction, covFunction=covFunction, inputs=x, params=.3, data=c(), packages='fields')

remoteCalcChol(matName='C', cholName='L', matPos='prob', cholPos='prob', n=size, h=prob$h_n) #+++++++++++++++ NOTE: chol.args is not used here and pivoting is not implemented
L <- collectTriangularMatrix('L', objPos = 'prob', n = size, h = prob$h_n) #+++++++++++++ NOTE: this is lower triangular, not upper triangular as returned by chol()
C<- collectRectangularMatrix('C', objPos = 'prob', n1=size, n2=size, h1=prob$h_n, h2=prob$h_n)
print(C)
L2 = t(chol(C))
all.equal(L, L2, scale=1)
bigGP.exit()
bigGP.quit(save = "no")
