library(Rmpi)
library(fields)
library(bigGP)

# In case R unexpectedly quits, clean up Rmpi processes and memory
  .Last <- function(){
    if (is.loaded("mpi_initialize")){
      if (mpi.comm.size(0) > 0){
        print("Use mpi.close.Rslaves() to close slaves.")
        mpi.close.Rslaves()
      }
      print("Use mpi.quit() to quit R")
      .Call("mpi_finalize")
    }
  }

mpi.spawn.Rslaves(nslaves = 15, needlog=F)

mpi.remote.exec(.libPaths())
mpi.remote.exec(library(fields))
mpi.remote.exec(library(bigGP))

.Last()
