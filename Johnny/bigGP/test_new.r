library(Rmpi)
library(fields)
require(bigGP, lib.loc='/glade/u/home/jpaige/code/')
require(rlecuyer)

#get command line arguments:
args <- commandArgs(trailingOnly = TRUE)
D = as.numeric(args[2])
print(args)
print(D)

#This function estimates the optimal block replication factor for Cholesky decomposition:
calcH = function(n, D){
  'Description:
Calculates a good choice of the block replication factor given the number of values.

n: numeric. The number of values being subdivided amongst the processes and blocks.
D: the number of blocks per length of the matrix

Value:
numeric. The value of H, the block replication factor.'
  
  return(max(n %/% (D * 1000), 1))
}

#input functions to initialize krigeProblem:
predMeanFunction = function(params, inputs, indices=T, cached=NULL) {1}
crossCovFunction = function(params, inputs, indices=T, cached=NULL) {1}
predCovFunction = function(params, inputs, indices=T, cached=NULL) {1}
meanFunction = function(params, inputs, indices=T, cached=NULL) {0}
covFunction = function(params, inputs, indices=T, cached=NULL) {
  vnorm = function(v,p=2) {
    if ( p =="I")
      return(max(abs(v)))
    else
      return(sum(abs(v)^p)^(1/p))
  }
  
  if(is.null(cached))
    cachedIsNull = TRUE else cachedIsNull = FALSE
  if(cachedIsNull){
    cached = list()
    
    cached$dists <- apply(matrix(inputs$coords[indices[,1],] - inputs$coords[indices[,2],], ncol=2), 1, vnorm)
  }
  dists <- cached$dists
  
  #calculate covariance using fields' Exponential(d, range = 1, alpha = 1/range, phi = 1)
  return(Exponential(dists, alpha=params[1]))
}

#Function used to calculate Cholesky decomposition with bigGP
#prob and probname: global krigeProblem and name of problem on the slaves respectively
chol_bigGP = function() {
  #calculate Cholesky decomposition:
  print('Calculating Cholesky decomposition')
  remoteCalcChol(matName='C', cholName='L', matPos=probName, cholPos=probName, n=prob$n, h=prob$h_n)
  
  #collect Cholesky Decomposition:
  print('Collecting Cholesky decomposition')
  L <- collectTriangularMatrix('L', objPos = probName, n = prob$n, h = prob$h_n)
  print('Finished collecting Cholesky decomposition')
  
  #return decomposition
  return(L)
}

#Function clears all bigGP objects from slaves and sets current to FALSE:
clearFromSlaves = function() {
  objects = remoteLs()
  remoteRm(objects)
  
  #TODO: Set current to FALSE
  prob$covCurrent = F
  prob$cholCurrent = F
}

#The following script computes how long the cholesky decomposition takes
#for different numbers of cores:

#intitialize loop variables:
nReps = 1
minSize = 30000
maxSize = 30000
sizes = round(seq(minSize, maxSize, length=nReps))
nSlaves = D*(D+1)/2

times = double(nReps)

for(r in 1:nReps) {
  probName <<- paste0('prob', r) #make a unique problem each iteration
  size = sizes[r]
  
  #only set doInit too TRUE on first time around (if doInit == T, krigeProblem$new calls bigGP.init)
  doInit = r == 1
  
  #make new krigeProblem, new observation locations:
  print('Making new Krig problem')
  x <- list(coords=matrix(runif(size*2), ncol=2, nrow=size))
  prob <<- krigeProblem$new(probName, h_n=NULL, numProcesses=nSlaves,
                          n=size, m=0, h_m=NULL, predMeanFunction = predMeanFunction,
                          crossCovFunction=crossCovFunction,
                          predCovFunction=predCovFunction, meanFunction=meanFunction,
                          covFunction=covFunction, inputs=x, params=.3, data=c(),
                          packages='fields', doInit = doInit)
  
  print(paste('rep:', r, 'size:', size, 'prob$n:', prob$n, 'prob$h_n:', prob$h_n)) 
  
  #time bigGP Cholesky decomposition:
  times[r] = system.time(L <- chol_bigGP())[3]
  print(times[r])
  
  #remove results from slaves:
  clearFromSlaves()
}

#save results and quit:
#save(list=c("nSlaves", "times"), file=paste("~/code/R/bigGP/bigGPTimes_n", 
                           #                                   maxSize, "_p", nSlaves + 1, ".RData", sep=''))
bigGP.quit(save = "no")

#Number of slaves must be D(D+1)/2 for some integer D:
#D = 2:floor((-1 + sqrt(1 + 8*maxSlaves))/2) #based on quadratic equation
#nSlaves = D*(D+1)/2
