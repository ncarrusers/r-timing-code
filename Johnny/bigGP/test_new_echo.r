args <- commandArgs(TRUE)
print(args)
srcFile <- args[1]
D <- as.numeric(args[2])
outFile <- paste0('test_new_D', D, '.Rout')
args <- args[-1]

sink(outFile, split = TRUE)
source(srcFile, echo = TRUE)
