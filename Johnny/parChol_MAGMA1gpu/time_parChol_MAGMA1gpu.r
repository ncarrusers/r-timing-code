library(fields)
library(Rmpi)
source('~/code/R/Rwrapper_MAGMA/Rwrapper_MAGMA.r')

#This script calculates the time required to compute many default Cholesky decompositions

#All processors calculate covariance matrix (Sigma) and its Cholesky decomp
processorTask = function(x) {
  #NOTE: Sigma, np, Tmatrix, y,  is sent beforehand
  gpuRank = mpi.comm.rank()-1
  if(gpuRank < 0)
    gpuRank = 0
  L = magmaChol_1gpu(Sigma, gpuRank)

  #####calculate likelihood:
  #first part:
  lnDetCov <- 2 * sum(log(diag(L)))

  #second part:
  VT <- forwardsolve(L, x=Tmatrix, transpose = FALSE, upper.tri = FALSE)
  qr.VT <- qr(VT)
  d.coef <- as.matrix(qr.coef(qr.VT, forwardsolve(L, transpose = FALSE,
                                                  y, upper.tri = FALSE)))
  c.coef <- as.matrix(forwardsolve(L, transpose = FALSE, y -
                                     Tmatrix %*% d.coef, upper.tri = FALSE))
  quad.form <- c(colSums(as.matrix(c.coef^2)))
  rho.MLE <- quad.form/np

  return(-np/2 - log(2 * pi) * (np/2) - (np/2) * log(rho.MLE) - lnDetCov/2)
}

##### set up problem:
nTimes = 16

genCovMat = function() {
#Assume lonlat has alred been sent
theta = 8 #exponential range parameter
distanceMatrix<- rdist(lonlat, lonlat)
# an exponential covariance function
return(Sigma<- exp(-distanceMatrix/theta))
}

parLnLik = function() {
#This function sends the necessary data to the slaves and collects the calculated cholesky decomp

#spawn slaves:
nslaves = 2
commTime = 0
commTime = commTime + system.time(mpi.spawn.Rslaves(nslaves = nslaves, needlog=F))[3]

#send MAGMA function, covariance mat, and task function to slaves (don't bcast because in general not same matrix):
commTime = commTime + system.time(mpi.bcast.cmd(library(fields)))[3]
commTime = commTime + system.time(mpi.bcast.cmd(source('~/code/R/Rwrapper_MAGMA/Rwrapper_MAGMA.r')))[3]
commTime = commTime + system.time(mpi.bcast.Robj2slave(lonlat))[3]
commTime = commTime + system.time(mpi.bcast.Robj2slave(y))[3]
commTime = commTime + system.time(mpi.bcast.Robj2slave(processorTask))[3]
commTime = commTime + system.time(mpi.bcast.cmd(np <- nrow(lonlat)))[3]

#generate covariance matrix on slaves:
commTime = commTime + system.time(mpi.bcast.Robj2slave(genCovMat))[3]
mpi.bcast.cmd(Sigma <- genCovMat())
#construct Tmatrix on slaves:
m <- 2
commTime = commTime + system.time(mpi.bcast.Robj2slave(m))[3]
commTime = commTime + system.time(mpi.bcast.cmd(Tmatrix <- fields.mkpoly(lonlat, m)))[3]

#run parallel computation:
#print('proc times:')
procTime <- system.time(out <- mpi.iapplyLB(1:nTimes, processorTask))[3]

#end:
commTime = commTime + system.time(mpi.close.Rslaves())[3]
return(list(commTime = commTime, procTime = procTime, lnLiks=out)) #NOTE: Ls should not be returned unless debugging
}

#get data:
data(CO2)
#lim <- 119 #limit for how many degrees lat/lon points can be from each other
maxSize = 15000 # maximum matrix size (number of observations) for timing
limIter = 3   # how far to increase lat/lon limits on CO2 data per iteration
lim <- 15     # starting limit for how many degrees lat/lon points can be from each other
i = 1
lims = c()
ns = c()
times = c()
while(T) {
  #only use a subset of the data that is close enough to 0 latitude and 0 longitude:
  ind <- (-lim < CO2$lon.lat[,1]) & (CO2$lon.lat[,1] < lim) # only include pts with lon coords between -Xlim and Xlim
  ind <- (ind & -lim/2 < CO2$lon.lat[,2]) & (CO2$lon.lat[,2] < lim/2) # only include pts with lat coords between -Xlim/2 and Xlim/2
  
  lonlat <- CO2$lon.lat[ind,]
  y <- CO2$y[ind]
  np <- nrow(lonlat)
  
  #if there's the same number of points as last time, continue:
  if(i > 1 && nrow(lonlat) == ns[i - 1]) {
    lim = lim + limIter
    next
  } else if(nrow(lonlat) > maxSize || lim > 180) {
    # if there are too many points in the matrix or all the points on the earth are covered, break:
    break
  }
  
  print(paste0('Using ', nrow(lonlat), ' data points with lim: ', lim))
  
  # else, record timings:
  ns[i] = nrow(lonlat)
  lims[i] = lim
  times[i] = system.time(out <- parLnLik())[3]
  
  print('lnLiks:')
  print(matrix(out$lnLiks, ncol=length(out$lnLiks)))
  
  # iterate:
  lim = lim + limIter
  i = i + 1
}

save(list=c('ns', 'lims', 'times'), file=paste0("~/code/R/parChol_MAGMA1gpu/time_parChol_MAGMA1gpu_n", maxSize, "_fullLik.RData"))

#Ls = out$Ls
#for(i in 1:length(Ls)) {\
#  L = Ls[[i]]
#  if(class(L) != 'matrix') {
#    print(i)
#    print(L)
#  }
#}
