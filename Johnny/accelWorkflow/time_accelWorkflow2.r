library(fields)
source('/picnic/u/home/jpaige/code/R/accelMKrig.MLE/myMKrig.MLE.r')

#The following script computes how long the Krig and mKrig takes for many sized matrices
#using the CO2 dataset from fields.  Uses chordal distances.

# convert latitude and longitude to 3D ECEF coordinates assuming all points are at sea-level:
LLA_to_ECEF = function(lat, lon, convertToRad=F) {
  # sources:
  # http://www.mathworks.de/help/toolbox/aeroblks/llatoecefposition.html
  # http://stackoverflow.com/questions/10473852/convert-latitude-and-longitude-to-point-in-3d-space
  
  f  = 1.0/298.257223563             # flattening to account for Earth as oblate spheroid
  ls = atan((1 - f)^2 * tan(lat))    # lambda, the mean sea-level
  R = 6378137                        # Equatorial radius of Earth (in meters)
  
  # Convert lat and lon to radians:
  if(convertToRad) {
    lat = pi/180*lat
    lon = pi/180*lon
  }
  
  #assumes all points are at sea-level:
  cosLat = cos(lat)
  sinLat = sin(lat)
  
  rad = R/sqrt(1 + (1/(1 - f)^2 - 1)*sin(ls)^2) # mean sea-level radius
  
  
  x = rad*cosLat * cos(lon)
  y = rad*cosLat * sin(lon)
  z = rad*sinLat
  
  return(cbind(x, y, z))
}

workflow = function(lonlat, y) {
  theta = 8
  
  print('optimizing over lambda')
  #now calculate likelihood for different lambdas using mKrig (requires lat and long data):
  lambda = 10^seq(-5, 0, length=10)
  
  out = myMKrig.MLE(lonlat, y, theta=theta, lambda=lambda, cov.args= list(Covariance="Exponential", Distance="rdist"), lambda.profile=FALSE, MAGMA_n=2)
  lnLikes = out$summary[,2]
  #for(i in 1:length(lambda)) {
  #  lam = lambda[i]
  #  print(paste('lambda:', lam))
  #  out = mKrig(lonlat, y, theta=theta, lambda=lam, cov.args= list(Covariance="Exponential", Distance="rdist.earth"))
  #  lnLikes[i] = out$lnProfileLike
  #}
  
  #use spline interpolator to find max likelihood:
  interpGrid = 10^(seq(-5, 0, length=150))
  interpLnLikes = splint(lambda, lnLikes, interpGrid)
  index = which.max(interpLnLikes)
  lambdaMLE <- interpGrid[index]
  
  #compute MLE krig:
  out.mle <- myMKrig(lonlat, y, theta=theta, lambda=lambdaMLE, cov.args= list(Covariance="Exponential", Distance="rdist"), MAGMA_n=2)
  
  #Now predict surfaces and exact errors using Krig and mKrig objects:
  #NOTE: computing Kriging surfaces doesn't work for 3D coordinates
  print('computing Kriging surfaces...')
  out.p <- predictSurface(out.mle)
  
  #world(add=TRUE)
  
  #predict approximate surfaces and errors using mKrig:
  print('approximating Kriging surface')
  minLat = min(lonlat[,2])
  maxLat = max(lonlat[,2])
  size = maxLat - minLat
  gridExpansion = max(c(1 + 1e-07, 10*theta/size))

  #make sure grid is at least 10 times as big as theta
  tmp <- try(sim.mKrig.approx(out.mle, M=5, gridExpansion=gridExpansion))
  if(class(tmp) == 'try-error') {
    print('Used extra large gridExpansion')
    out.sim <- sim.mKrig.approx(out.mle, M=5, gridExpansion=2*gridExpansion)
    print('Finished using extra large grid expansion')
  } else {
    out.sim <- tmp
  }
  
  invisible(NULL)
}

#now time the workflow for Krig or mKrig using CO2 dataset from fields:
data(CO2)

maxSize = 15000 # maximum matrix size (number of observations) for timing
limIter = 3   # how far to increase lat/lon limits on CO2 data per iteration
lim <- 15     # starting limit for how many degrees lat/lon points can be from each other
i = 1

lims = c()
ns = c()
times = c()
while(T) {
  #only use a subset of the data that is close enough to 0 latitude and 0 longitude:
  ind <- (-lim < CO2$lon.lat[,1]) & (CO2$lon.lat[,1] < lim) # only include pts with lon coords between -Xlim and Xlim
  ind <- (ind & -lim/2 < CO2$lon.lat[,2]) & (CO2$lon.lat[,2] < lim/2) # only include pts with lat coords between -Xlim/2 and Xlim/2
  
  lonlat = CO2$lon.lat[ind,]
  y <- CO2$y[ind]
  
  print(paste0('Using ', length(y), ' data points with lim: ', lim))
  
  #if there's the same number of points as last time, continue:
  if(i > 1 && length(y) == ns[i - 1]) {
    lim = lim + limIter
    next
  } else if(length(y) > maxSize || lim > 180) {
    # if there are too many points in the matrix or all the points on the earth are covered, break:
    break
  }
  
  # else, record timings:
  ns[i] = length(y)
  lims[i] = lim
  
  times[i] = system.time(workflow(lonlat, y))[3]
  
  # iterate:
  lim = lim + limIter
  i = i + 1
}

#save data:
save(list=c("ns", "times", "lims"), file=paste0("~/code/R/accelWorkflow/accelMKrigWorkflowTimes2_n", maxSize, ".RData"))
