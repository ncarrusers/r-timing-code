
R version 3.0.1 (2013-05-16) -- "Good Sport"
Copyright (C) 2013 The R Foundation for Statistical Computing
Platform: x86_64-unknown-linux-gnu (64-bit)

R is free software and comes with ABSOLUTELY NO WARRANTY.
You are welcome to redistribute it under certain conditions.
Type 'license()' or 'licence()' for distribution details.

  Natural language support but running in an English locale

R is a collaborative project with many contributors.
Type 'contributors()' for more information and
'citation()' on how to cite R or R packages in publications.

Type 'demo()' for some demos, 'help()' for on-line help, or
'help.start()' for an HTML browser interface to help.
Type 'q()' to quit R.

> library(fields)
Loading required package: spam
Loading required package: grid
Spam version 0.41-0 (2014-02-26) is loaded.
Type 'help( Spam)' or 'demo( spam)' for a short introduction 
and overview of this package.
Help for individual functions is also obtained by adding the
suffix '.spam' to the function name, e.g. 'help( chol.spam)'.

Attaching package: 'spam'

The following object is masked from 'package:base':

    backsolve, forwardsolve

Loading required package: maps
Warning messages:
1: package 'fields' was built under R version 3.1.0 
2: package 'spam' was built under R version 3.1.0 
3: package 'maps' was built under R version 3.1.0 
> source('/picnic/u/home/jpaige/code/R/accelMKrig.MLE/myMKrig.MLE.r')
> 
> #The following script computes how long the Krig and mKrig takes for many sized matrices
> #using the CO2 dataset from fields.  Uses chordal distances.
> 
> # convert latitude and longitude to 3D ECEF coordinates assuming all points are at sea-level:
> LLA_to_ECEF = function(lat, lon, convertToRad=F) {
+   # sources:
+   # http://www.mathworks.de/help/toolbox/aeroblks/llatoecefposition.html
+   # http://stackoverflow.com/questions/10473852/convert-latitude-and-longitude-to-point-in-3d-space
+   
+   f  = 1.0/298.257223563             # flattening to account for Earth as oblate spheroid
+   ls = atan((1 - f)^2 * tan(lat))    # lambda, the mean sea-level
+   R = 6378137                        # Equatorial radius of Earth (in meters)
+   
+   # Convert lat and lon to radians:
+   if(convertToRad) {
+     lat = pi/180*lat
+     lon = pi/180*lon
+   }
+   
+   #assumes all points are at sea-level:
+   cosLat = cos(lat)
+   sinLat = sin(lat)
+   
+   rad = R/sqrt(1 + (1/(1 - f)^2 - 1)*sin(ls)^2) # mean sea-level radius
+   
+   
+   x = rad*cosLat * cos(lon)
+   y = rad*cosLat * sin(lon)
+   z = rad*sinLat
+   
+   return(cbind(x, y, z))
+ }
> 
> workflow = function(lonlat, y) {
+   theta = 8
+   
+   print('optimizing over lambda')
+   #now calculate likelihood for different lambdas using mKrig (requires lat and long data):
+   lambda = 10^seq(-5, 0, length=10)
+   
+   out = myMKrig.MLE(lonlat, y, theta=theta, lambda=lambda, cov.args= list(Covariance="Exponential", Distance="rdist"), lambda.profile=FALSE, MAGMA_n=2)
+   lnLikes = out$summary[,2]
+   #for(i in 1:length(lambda)) {
+   #  lam = lambda[i]
+   #  print(paste('lambda:', lam))
+   #  out = mKrig(lonlat, y, theta=theta, lambda=lam, cov.args= list(Covariance="Exponential", Distance="rdist.earth"))
+   #  lnLikes[i] = out$lnProfileLike
+   #}
+   
+   #use spline interpolator to find max likelihood:
+   interpGrid = 10^(seq(-5, 0, length=150))
+   interpLnLikes = splint(lambda, lnLikes, interpGrid)
+   index = which.max(interpLnLikes)
+   lambdaMLE <- interpGrid[index]
+   
+   #compute MLE krig:
+   out.mle <- myMKrig(lonlat, y, theta=theta, lambda=lambdaMLE, cov.args= list(Covariance="Exponential", Distance="rdist"), MAGMA_n=2)
+   
+   #Now predict surfaces and exact errors using Krig and mKrig objects:
+   #NOTE: computing Kriging surfaces doesn't work for 3D coordinates
+   print('computing Kriging surfaces...')
+   out.p <- predictSurface(out.mle)
+   
+   #world(add=TRUE)
+   
+   #predict approximate surfaces and errors using mKrig:
+   print('approximating Kriging surface')
+   minLat = min(lonlat[,2])
+   maxLat = max(lonlat[,2])
+   size = maxLat - minLat
+   gridExpansion = max(c(1 + 1e-07, 10*theta/size))
+ 
+   #make sure grid is at least 10 times as big as theta
+   tmp <- try(sim.mKrig.approx(out.mle, M=5, gridExpansion=gridExpansion))
+   if(class(tmp) == 'try-error') {
+     print('Used extra large gridExpansion')
+     out.sim <- sim.mKrig.approx(out.mle, M=5, gridExpansion=2*gridExpansion)
+     print('Finished using extra large grid expansion')
+   } else {
+     out.sim <- tmp
+   }
+   
+   invisible(NULL)
+ }
> 
> #now time the workflow for Krig or mKrig using CO2 dataset from fields:
> data(CO2)
> 
> maxSize = 15000 # maximum matrix size (number of observations) for timing
> limIter = 3   # how far to increase lat/lon limits on CO2 data per iteration
> lim <- 15     # starting limit for how many degrees lat/lon points can be from each other
> i = 1
> 
> lims = c()
> ns = c()
> times = c()
> while(T) {
+   #only use a subset of the data that is close enough to 0 latitude and 0 longitude:
+   ind <- (-lim < CO2$lon.lat[,1]) & (CO2$lon.lat[,1] < lim) # only include pts with lon coords between -Xlim and Xlim
+   ind <- (ind & -lim/2 < CO2$lon.lat[,2]) & (CO2$lon.lat[,2] < lim/2) # only include pts with lat coords between -Xlim/2 and Xlim/2
+   
+   lonlat = CO2$lon.lat[ind,]
+   y <- CO2$y[ind]
+   
+   print(paste0('Using ', length(y), ' data points with lim: ', lim))
+   
+   #if there's the same number of points as last time, continue:
+   if(i > 1 && length(y) == ns[i - 1]) {
+     lim = lim + limIter
+     next
+   } else if(length(y) > maxSize || lim > 180) {
+     # if there are too many points in the matrix or all the points on the earth are covered, break:
+     break
+   }
+   
+   # else, record timings:
+   ns[i] = length(y)
+   lims[i] = lim
+   
+   times[i] = system.time(workflow(lonlat, y))[3]
+   
+   # iterate:
+   lim = lim + limIter
+   i = i + 1
+ }
[1] "Using 193 data points with lim: 15"
[1] "optimizing over lambda"
[1] "computing Kriging surfaces..."
[1] "approximating Kriging surface"
[1] "Using 248 data points with lim: 18"
[1] "optimizing over lambda"
[1] "computing Kriging surfaces..."
[1] "approximating Kriging surface"
[1] "Using 343 data points with lim: 21"
[1] "optimizing over lambda"
[1] "computing Kriging surfaces..."
[1] "approximating Kriging surface"
[1] "Using 410 data points with lim: 24"
[1] "optimizing over lambda"
[1] "computing Kriging surfaces..."
[1] "approximating Kriging surface"
[1] "Using 555 data points with lim: 27"
[1] "optimizing over lambda"
[1] "computing Kriging surfaces..."
[1] "approximating Kriging surface"
[1] "Using 662 data points with lim: 30"
[1] "optimizing over lambda"
[1] "computing Kriging surfaces..."
[1] "approximating Kriging surface"
[1] "Using 849 data points with lim: 33"
[1] "optimizing over lambda"
[1] "computing Kriging surfaces..."
[1] "approximating Kriging surface"
[1] "Using 1018 data points with lim: 36"
[1] "optimizing over lambda"
[1] "computing Kriging surfaces..."
[1] "approximating Kriging surface"
[1] "Using 1258 data points with lim: 39"
[1] "optimizing over lambda"
[1] "computing Kriging surfaces..."
[1] "approximating Kriging surface"
[1] "Using 1445 data points with lim: 42"
[1] "optimizing over lambda"
[1] "computing Kriging surfaces..."
[1] "approximating Kriging surface"
[1] "Using 1756 data points with lim: 45"
[1] "optimizing over lambda"
[1] "computing Kriging surfaces..."
[1] "approximating Kriging surface"
[1] "Using 1993 data points with lim: 48"
[1] "optimizing over lambda"
[1] "computing Kriging surfaces..."
[1] "approximating Kriging surface"
[1] "Using 2399 data points with lim: 51"
[1] "optimizing over lambda"
[1] "computing Kriging surfaces..."
[1] "approximating Kriging surface"
[1] "Using 2622 data points with lim: 54"
[1] "optimizing over lambda"
[1] "computing Kriging surfaces..."
[1] "approximating Kriging surface"
[1] "Using 3067 data points with lim: 57"
[1] "optimizing over lambda"
[1] "computing Kriging surfaces..."
[1] "approximating Kriging surface"
[1] "Using 3333 data points with lim: 60"
[1] "optimizing over lambda"
[1] "computing Kriging surfaces..."
[1] "approximating Kriging surface"
[1] "Using 3768 data points with lim: 63"
[1] "optimizing over lambda"
[1] "computing Kriging surfaces..."
[1] "approximating Kriging surface"
[1] "Using 4140 data points with lim: 66"
[1] "optimizing over lambda"
[1] "computing Kriging surfaces..."
[1] "approximating Kriging surface"
[1] "Using 4604 data points with lim: 69"
[1] "optimizing over lambda"
[1] "computing Kriging surfaces..."
[1] "approximating Kriging surface"
[1] "Using 4981 data points with lim: 72"
[1] "optimizing over lambda"
[1] "computing Kriging surfaces..."
[1] "approximating Kriging surface"
[1] "Using 5473 data points with lim: 75"
[1] "optimizing over lambda"
[1] "computing Kriging surfaces..."
[1] "approximating Kriging surface"
[1] "Using 5801 data points with lim: 78"
[1] "optimizing over lambda"
[1] "computing Kriging surfaces..."
[1] "approximating Kriging surface"
[1] "Using 6329 data points with lim: 81"
[1] "optimizing over lambda"
[1] "computing Kriging surfaces..."
[1] "approximating Kriging surface"
[1] "Using 6626 data points with lim: 84"
[1] "optimizing over lambda"
[1] "computing Kriging surfaces..."
[1] "approximating Kriging surface"
[1] "Using 7202 data points with lim: 87"
[1] "optimizing over lambda"
[1] "computing Kriging surfaces..."
[1] "approximating Kriging surface"
[1] "Using 7563 data points with lim: 90"
[1] "optimizing over lambda"
[1] "computing Kriging surfaces..."
[1] "approximating Kriging surface"
[1] "Using 8041 data points with lim: 93"
[1] "optimizing over lambda"
[1] "computing Kriging surfaces..."
[1] "approximating Kriging surface"
[1] "Using 8507 data points with lim: 96"
[1] "optimizing over lambda"
[1] "computing Kriging surfaces..."
[1] "approximating Kriging surface"
[1] "Using 8990 data points with lim: 99"
[1] "optimizing over lambda"
[1] "computing Kriging surfaces..."
[1] "approximating Kriging surface"
[1] "Using 9436 data points with lim: 102"
[1] "optimizing over lambda"
[1] "computing Kriging surfaces..."
[1] "approximating Kriging surface"
[1] "Using 9943 data points with lim: 105"
[1] "optimizing over lambda"
[1] "computing Kriging surfaces..."
[1] "approximating Kriging surface"
[1] "Using 10311 data points with lim: 108"
[1] "optimizing over lambda"
[1] "computing Kriging surfaces..."
[1] "approximating Kriging surface"
[1] "Using 10933 data points with lim: 111"
[1] "optimizing over lambda"
[1] "computing Kriging surfaces..."
[1] "approximating Kriging surface"
[1] "Using 11321 data points with lim: 114"
[1] "optimizing over lambda"
[1] "computing Kriging surfaces..."
[1] "approximating Kriging surface"
[1] "Using 12002 data points with lim: 117"
[1] "optimizing over lambda"
[1] "computing Kriging surfaces..."
[1] "approximating Kriging surface"
[1] "Using 12396 data points with lim: 120"
[1] "optimizing over lambda"
[1] "computing Kriging surfaces..."
[1] "approximating Kriging surface"
[1] "Using 12931 data points with lim: 123"
[1] "optimizing over lambda"
[1] "computing Kriging surfaces..."
[1] "approximating Kriging surface"
[1] "Using 13491 data points with lim: 126"
[1] "optimizing over lambda"
[1] "computing Kriging surfaces..."
[1] "approximating Kriging surface"
[1] "Using 14068 data points with lim: 129"
[1] "optimizing over lambda"
[1] "computing Kriging surfaces..."
[1] "approximating Kriging surface"
[1] "Using 14658 data points with lim: 132"
[1] "optimizing over lambda"
[1] "computing Kriging surfaces..."
[1] "approximating Kriging surface"
[1] "Using 15338 data points with lim: 135"
> 
> #save data:
> save(list=c("ns", "times", "lims"), file=paste0("~/code/R/accelWorkflow/accelMKrigWorkflowTimes2_n", maxSize, ".RData"))
> 
> proc.time()
    user   system  elapsed 
4705.889 1065.463 5787.495 
