#include <R.h>
#include <Rdefines.h>
#include <cula_lapack.h>
void checkStatus(culaStatus status)
{
        char buf[256];
        if(!status)
          return;

        culaGetErrorInfoString(status, culaGetErrorInfo(), buf, sizeof(buf));
        printf("%s\n", buf);
}

SEXP culaEigen(SEXP A, SEXP w, SEXP n)
{
	int In;
	double *PA, *Pw;
	In = INTEGER_VALUE(n);
	PA = NUMERIC_POINTER(A);
	Pw = NUMERIC_POINTER(w);
	culaStatus status;
	culaDouble *L = PA;
	culaDouble *arr = Pw;

	status = culaInitialize();
	checkStatus(status);
	status = culaDsyev('V','L',In,L,In, arr);
	checkStatus(status);
	culaShutdown();
	return(R_NilValue);
}
