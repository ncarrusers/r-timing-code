#include <stdlib.h>
#include <stdio.h>
#include <R.h>
#include <Rdefines.h>
#include "magma.h"
#include <magma_lapack.h>

extern "C" SEXP magmaCholesky_mic(SEXP A, SEXP n, SEXP NB)
{
	int In, INB;
	double *PA, *Ptime;
	In = INTEGER_VALUE(n);
	INB = INTEGER_VALUE(NB);
	PA = NUMERIC_POINTER(A);
	magma_int_t N, info;
	N=In;
	
	magma_queue_t queue;
	magma_device_t device;
	magma_int_t num = 0;
	magma_int_t err;
	magma_init();
	err = magma_get_devices(&device,1,&num);
	if(err != MAGMA_SUCCESS || num < 1)
	{
		fprintf(stderr,"magma_get_devices failed: %d\n", (int) err);
		exit(-1);
	}
	err = magma_queue_create(device,&queue);
	if(err != MAGMA_SUCCESS || num < 1)
	{
		fprintf(stderr,"magma_queue_create failed: %d\n", (int) err);
		exit(-1);
	}
	int i,j;
	double *dA,*hA;
	magma_int_t ldda;
	ldda = ((N+31)/32)*32;

	magma_malloc_pinned((void**)&hA, In*In*sizeof(double)); 
	if ( MAGMA_SUCCESS !=                                                  
            magma_malloc( (void**) &dA, ldda*In*sizeof(double) ) ) {         
	printf("error in magma_malloc");
        magma_finalize();                                                  
        exit(-1);                                                          
   	}
	
	for(i=0; i<In*In; i++) {
		hA[i] = PA[i];
	}	

	magma_dsetmatrix(N, N, hA, 0, N, dA, 0, ldda, queue);
	magma_dpotrf_mic(MagmaLower, N, dA, 0, ldda, &info, queue);
	if (info != 0)
        printf("magma_dpotrf_mic returned error %d.\n", (int) info);
	magma_dgetmatrix(N, N, dA, 0, ldda, hA, 0, N, queue);

	for(i=0; i < In*In; i++)
	{
		PA[i] = hA[i];
	}

	magma_free(dA);
	magma_free_pinned(hA);
	magma_queue_destroy(queue);
	magma_finalize();

	for(i = 1; i < In; i++)
        {
                for(j=0; j< i; j++)
                {
                        PA[i*In+j] = 0.0;
                }
        }
	return(R_NilValue);
}
	
	
	
	
