#include <R.h>
#include <Rinternals.h>
#include <R_ext/Arith.h>
#include <Rmath.h>
#include <float.h>
SEXP compactToMatC(SEXP compactMat, SEXP len, SEXP n, SEXP diagVal)
{
  int In, Ilen;
  double diagVal;
  double *cMat;
  
  In = INTEGER(n)[0];
  Ilen = INTEGER(len)[0];
  dVal = REAL(diagVal)[0];
  cMat = REAL(compactMat);
  SEXP ans = PROTECT(allocMatrix(REALSXP, In, In));
  
  F77_CALL(compactToMat)(&ans, cMat, Ilen, In, dVal);
  UNPROTECT(1);
  return ans;
}