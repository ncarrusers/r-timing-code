c fields, Tools for spatial data
c Copyright 2004-2013, Institute for Mathematics Applied Geosciences
c University Corporation for Atmospheric Research
c Licensed under the GPL -- www.gpl.org/licenses/gpl.html

c
c evaluates exponential covariance function over upper triangular part of input matrix
c

      subroutine ExponentialUpper(distMat, d1, d2, alpha, phi)
      integer d1, d2
      double precision distMat(d1, d2), alpha, phi
      
      integer i, j
      do 10 i = 1, d1
          do 20 j = i, d2
              distMat(i, j) = phi*exp(-1*distMat(i, j)*alpha)
   20     continue
   10 continue

      return
      end
