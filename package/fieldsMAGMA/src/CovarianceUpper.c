#include <R.h>
#include <Rinternals.h>

SEXP CovarianceUpper(SEXP covFun, SEXP mat, SEXP n, SEXP diagVal, SEXP REnv) {
  int In;
  double dVal;
  In = INTEGER(n)[0];
  dVal = REAL(diagVal)[0];
  double *Pmat = NUMERIC_POINTER(mat);
  double *ans = PROTECT(calloc(In*In, sizeof(double)));
  
  SEXP fieldsPackage;
  PROTECT(
    fieldsPackage = eval(lang2(install("getNamespace"),
                               ScalarString(mkChar("fields"))),
                         REnv
    )   
  );  
  
  SEXP R_fcall = PROTECT(lang2(covFun, R_NilValue)); /* could allocate once and preserve for re-use */
  SEXP arg = PROTECT(install("x"));
  
  //loop over upper triangle of matrix, evaluating
  int i, j;
  for(i = 0; i < In; i++) {
    for(j = 0; j <= i; j++) {
      if(i == j)
        ans[i*In + j] = dVal;
      else {
        defineVar(arg, Pmat[i*In + j], REnv);
        SETCADR(R_fcall, arg);
        ans[i*In + j] = REAL(eval(R_fcall, REnv))[0];
      }
    }
  }
  
  UNPROTECT(3); 
  return(ans);
}