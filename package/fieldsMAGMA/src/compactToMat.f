c fields, Tools for spatial data
c Copyright 2004-2013, Institute for Mathematics Applied Geosciences
c University Corporation for Atmospheric Research
c Licensed under the GPL -- www.gpl.org/licenses/gpl.html

c
c converts matrix in compact vector form to two dimensional array
c

      subroutine compactToMat(mat, compactMat, len, n, diagVal)
      integer len, n
      double precision compactMat(len), mat(n, n)
      
      integer i, j, ind
      ind = 1
      do i = 1, n
          do j = 1, i
              if(i .eq. j) then
                  mat(i, i) = diagVal
              else
                  mat(i, j) = compactMat(ind)
                  ind = ind + 1
              endif
          continue
      continue

      return
      end
