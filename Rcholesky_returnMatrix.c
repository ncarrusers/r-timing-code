/* Function to run cholesky decomp through R using CULA library
returns a seperate lower triangular matrix */
/* Example usage in R where vec = matrix in column major form, n = dimension
   dyn.load("Rcholesky.so")
  L = .Call("gpuCholesky",vec,n)

*/
#include <R.h>
#include <Rdefines.h>
#include <cula_lapack.h>

SEXP gpuCholesky(SEXP A, SEXP n)
{
	/* Set R variables in C */
	int In;
	double *PA;
	PROTECT(In = AS_INTEGER(n));
	In = INTEGER_VALUE(n);
	PA = NUMERIC_POINTER(A);
	
	int i,j;	
	culaStatus status;
	culaDouble *L = PA;
	
	/* Run cholesky decomp on GPU with L */
	status = culaInitialize();
	status = culaDpotrf('L', In, L, In);
	culaShutdown();
	
	/* Set upper triangular to zeros */
	for(i = 1; i < In; i++)
	{
		for(j=0; j< i; j++)
		{
			L[i*In+j] = 0.0;
		}
	}

	/* Create L matrix to be returned to R */
	SEXP Lreturn;
	double *p_Lreturn;
	PROTECT(Lreturn = NEW_NUMERIC(In*In));
	p_Lreturn = NUMERIC_POINTER(Lreturn);
	for(i = 0; i < In*In; i++)
	{
		p_Lreturn[i] = L[i];
	}

	free(L);
	UNPROTECT(2);
	return Lreturn;
}
	
	
